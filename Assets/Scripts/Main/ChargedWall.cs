using System.Collections;
using UnityEngine;

[RequireComponent(typeof(DOTAnimations))]
public class ChargedWall : MonoBehaviour
{
    [SerializeField] private GameObjectRuntimeSet _playerSet;

    private BounceCharge _bounceCharge;
    private bool _charged = true;
    private DOTAnimations _DOTAnimations;

    private void Start()
    {
        _bounceCharge = _playerSet.GetItemIndex(0).GetComponent<BounceCharge>();
        _DOTAnimations = GetComponent<DOTAnimations>();
    }

    public bool TryIncreasePlayerCharge()
    {
        if (_charged)
        {
            _bounceCharge.IncreaseValue(1);
            _charged = false;
            new Task(RefillWall());
            return true;
        } else
        {
            return false;
        }
    }

    private IEnumerator RefillWall()
    {
        // ������ ��������
        new Task(_DOTAnimations.TransitBodyColor(Color.white, 0.2f));

        yield return new WaitForSeconds(5f);

        // ���������� ��������
        new Task(_DOTAnimations.TransitBodyColor(new Color(0.2915f, 0.9811f, 0.5750f), 0.2f));
        _charged = true;
    }
}
