using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class CameraController : MonoBehaviour
{
    [SerializeField] private GameObjectRuntimeSet _currentArenaControllerRuntimeSet;
    [SerializeField] private GameObjectRuntimeSet _gameManagerRuntimeSet;
    [SerializeField] private GameObject _endLevelMenuGameObject;
    [SerializeField] private GameObject[] _HUDGameObjects;
    private Animator _animator;

    private void Awake()
    {
        _animator = GetComponent<Animator>();
    }

    public void PlayExitArenaAnimation()
    {
        _animator.SetTrigger("ExitArena");
    }

    public void PlayExitLevelAnimation()
    {
        _animator.SetTrigger("ExitLevel");
    }

    public void PlayEnterArenaAnimation()
    {
        _animator.SetTrigger("EnterArena");
    }

    public void PlayEnterLevelAnimation()
    {
        _animator.SetTrigger("EnterLevel");
    }

    public void PlayZoomInCutsceneAnimation()
    {
        _animator.SetBool("IsZoomed", true);
    }

    public void PlayZoomOutCutsceneAnimation()
    {
        _animator.SetBool("IsZoomed", false);
    }

    public void PlayFadeInDeathAnimation()
    {
        _animator.SetBool("IsFaded", true);
    }

    public void PlayFadeOutDeathAnimation()
    {
        _animator.SetBool("IsFaded", false);
    }

    private void LoadNextArena()
    {
        _currentArenaControllerRuntimeSet.GetItemIndex(0).GetComponent<ArenaController>().LoadNextArena();
    }

    private void EnterLevel()
    {
        _currentArenaControllerRuntimeSet.GetItemIndex(0).GetComponent<ArenaController>().EnterLevel();
    }

    private void TryPlayCutcsene()
    {
        _currentArenaControllerRuntimeSet.GetItemIndex(0).GetComponent<ArenaController>().TryPlayCutscene();
    }

    // ���������� ����������� � toggle
    private void TurnOnPlayerInput()
    {
        _currentArenaControllerRuntimeSet.GetItemIndex(0).GetComponent<ArenaController>().TogglePlayerInput(true);
    }

    private void TurnOffPlayerInput()
    {
        _currentArenaControllerRuntimeSet.GetItemIndex(0).GetComponent<ArenaController>().TogglePlayerInput(false);
    }

    private void ReloadArena()
    {
        _gameManagerRuntimeSet.GetItemIndex(0).GetComponent<GameManager>().ReloadScene();
    }

    private void CompleteLevel()
    {
        _endLevelMenuGameObject.SetActive(true);
        _gameManagerRuntimeSet.GetItemIndex(0).GetComponent<GameManager>().CompleteLevel();
    }

    public void ToggleHUD(int toggle)
    {
        foreach(GameObject HUDObject in _HUDGameObjects)
        {
            HUDObject.SetActive(Convert.ToBoolean(toggle));
        }
    }
}
