using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttackTrigger : MonoBehaviour
{
    [SerializeField] private float AttackPower;
    [SerializeField] private float KnockbackPower;

    private Enemy _enemy;
    private GameObject _playerGameObject;

    private void Awake()
    {
        _enemy = GetComponentInParent<Enemy>();
    }

    private void Start()
    {
        _playerGameObject = _enemy.PlayerSet.GetItemIndex(0);
    }

    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.GetComponentInParent<Player>() != null)
        {
            _playerGameObject.GetComponentInParent<Player>().Hit(AttackPower, KnockbackPower, transform.position);
        }
    }
}
