using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public abstract class EnemyState : MonoBehaviour
{
    public EnemyState NextState { get; protected set; }
    public bool Running { get; set; } // ���� ������ ���������.
    public virtual void Run()
    {
        Running = true;
    }

    protected Enemy Enemy;
    protected Rigidbody2D Rigidbody;
    protected EnemyStateMachine StateMachine;
    protected GameObject PlayerGameObject;
    protected DOTAnimations DOTAnimations;
    protected Animator Animator;
    protected NavMeshAgent _agent;
    protected Rigidbody2D _rigidbody;

    protected virtual void Awake()
    {
        Enemy = GetComponent<Enemy>();
        StateMachine = GetComponent<EnemyStateMachine>();
        Rigidbody = GetComponent<Rigidbody2D>();
        DOTAnimations = GetComponent<DOTAnimations>();
        Animator = GetComponent<Animator>();
        _agent = GetComponent<NavMeshAgent>();
        _rigidbody = GetComponent<Rigidbody2D>();
    }

    protected virtual void Start()
    {
        PlayerGameObject = Enemy.PlayerSet.GetItemIndex(0);
        _agent.updateRotation = false;
        _agent.updateUpAxis = false;
    }

    // ������ 2 ������ ��������� ������ � ������ ���������, �� ��� ������������ ����������� � ������ �
    // ����������� �������� �� � ������ ��������� ����� ������������ ����. ��� ���������?

    /*
    protected void CheckPlayerRange(float range)
    {
        if (Vector3.Distance(transform.position, PlayerGameObject.transform.position) <= range)
        {
            // �������� ��������� �� ���������� ������� � �������
            Debug.DrawRay(transform.position, new Vector3(Rigidbody.position.x, Rigidbody.position.y, 0) + _agent.velocity.normalized * 100f, Color.red);
            RaycastHit2D hit = Physics2D.Raycast(transform.position, new Vector3(Rigidbody.position.x, Rigidbody.position.y, 0) + _agent.velocity.normalized);
            //StateMachine.RunState(NextState);
            if (hit.collider.tag != "Wall")
            {
                Debug.Log(hit.collider.tag);
                Debug.Log(hit.collider.name);
                Debug.Log("NO WALL, ATTACK");
                StateMachine.RunState(NextState);
            }
        }
    }
    */

    protected void RotateToTarget(Vector3 targetPosition)
    {
        Quaternion rotation = Quaternion.LookRotation(Vector3.forward, targetPosition - transform.position);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, 5f * Time.deltaTime);
        _rigidbody.angularVelocity = 0f;
    }
}
