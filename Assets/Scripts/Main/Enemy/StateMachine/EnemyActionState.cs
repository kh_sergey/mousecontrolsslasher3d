using System;
using System.Collections;
using UnityEngine;
using DG.Tweening;

public abstract class EnemyActionState : EnemyState
{
    /*
    [SerializeField] protected float AttackPower;
    [SerializeField] protected float KnockbackPower;
    */

    public Task attackTask { get; protected set; }
    protected abstract void Attack();

    //protected abstract void OnTriggerEnter2D(Collider2D collider);
    public abstract void ToggleDamageTrigger(int colliderStatus); // int - ������ ��� ������� ���������� ����� ��������, � ��� ��� bool =)

    protected override void Awake()
    {
        base.Awake();
        NextState = GetComponent<EnemyMovementState>();
    }
}
