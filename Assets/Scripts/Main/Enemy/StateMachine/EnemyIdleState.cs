using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class EnemyIdleState : EnemyState
{
    [SerializeField] protected float _detectionRange;
    public float DetectionRange { get => _detectionRange; protected set => _detectionRange = value; }

    protected override void Awake()
    {
        base.Awake();
        NextState = GetComponent<EnemyMovementState>();
    }
}
