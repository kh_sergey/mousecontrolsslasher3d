using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class EnemyStaggerState : EnemyState
{
    [SerializeField] protected MeshRenderer BodyRenderer;
    protected Material BodyMaterial;
    public Task knockbackTask { get; protected set; }

    public void Initialize(float damageValue, float receivedKnockbackPower, Vector3 knockbackPointPosition, GameObject hittedObject)
    {
        _damageValue = damageValue;
        _receivedKnockbackPower = receivedKnockbackPower;
        _knockbackPointPosition = knockbackPointPosition;
        _hittedObject = hittedObject;
    }

    abstract protected void StartStaggerAnimation();
    abstract protected IEnumerator KnockbackTask(float receivedKnockbackPower, Vector3 knockbackPointPosition);

    protected float _damageValue;
    protected float _receivedKnockbackPower;
    protected Vector3 _knockbackPointPosition;
    protected GameObject _hittedObject;

    protected override void Awake()
    {
        base.Awake();
        NextState = GetComponent<EnemyMovementState>();
        BodyMaterial = BodyRenderer.material;
    }

    protected IEnumerator TakeDamage(float damageValue)
    {
        //Debug.Log("rabota");
        Enemy.Health.Damage(damageValue);
        if (damageValue > 0)
        {
            new Task(DOTAnimations.MakeHitColor());
            //DOTAnimations.MakeHitColor();
            Enemy.ShowDamagePopup(damageValue);
        }
        else
        {
            new Task(DOTAnimations.MakePushedColor());
            //DOTAnimations.MakePushedColor();
        }
        yield return new WaitForFixedUpdate(); // ������� DOTween'a
    }

    /*
    private void MakeHitColor() // ������-�� DOTween �������� ��������� ������ � IEnumerator. �������������� � ������ � ������� �������.
    {
        Debug.Log("hit task");
        StartCoroutine(DOTAnimations.MakeHitColor());
    }

    private void MakePushedColor()
    {
        Debug.Log("push task");
        StartCoroutine(DOTAnimations.MakePushedColor());
    }
    */
}
