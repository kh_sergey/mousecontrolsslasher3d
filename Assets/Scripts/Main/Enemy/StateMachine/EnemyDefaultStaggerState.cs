using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDefaultStaggerState : EnemyStaggerState
{
    public override void Run()
    {
        base.Run();
        _agent.enabled = false;
        StartStaggerAnimation();
        new Task(TakeDamage(_damageValue));
        GetKnockbacked(_knockbackPointPosition, _receivedKnockbackPower);
    }

    private void GetKnockbacked(Vector3 knockbackPointPosition, float receivedKnockbackPower)
    {
        Enemy.StopAllMovementTasks();
        knockbackTask = new Task(KnockbackTask(receivedKnockbackPower, knockbackPointPosition));
    }

    protected override IEnumerator KnockbackTask(float receivedKnockbackPower, Vector3 knockbackPointPosition)
    {
        Vector3 directionFromStaggerPoint = (transform.position - knockbackPointPosition).normalized;
        Rigidbody.velocity = Vector3.zero; // ��� ��� � ������� ������ ��� ��������� ��������� �� ������������
        Rigidbody.velocity += new Vector2(directionFromStaggerPoint.x, directionFromStaggerPoint.y) * receivedKnockbackPower;

        while (Mathf.Abs(Rigidbody.velocity.x) + Mathf.Abs(Rigidbody.velocity.y) > 1f)
        {
            Rigidbody.velocity = Rigidbody.velocity * 0.95f;

            yield return new WaitForFixedUpdate();
        }

        Rigidbody.velocity = Vector3.zero;
    }

    protected override void StartStaggerAnimation()
    {
        Animator.SetTrigger("Staggered");
    }

    public void EndStaggerState()
    {
        _agent.enabled = true;
        StateMachine.RunState(NextState);
    }
}
