using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShieldPreparationForActionState : EnemyPreparationForActionState
{
    [SerializeField] private Collider2D PhysicsCollider;

    public override void Run()
    {
        base.Run();
        PreparationForAttack();
    }

    private void FixedUpdate()
    {
        if (Running)
        {
            RotateToTarget(PlayerGameObject.transform.position);
        }
    }

    public override void PreparationForAttack()
    {
        if (preparationForAttackTask == null || !preparationForAttackTask.Running)
        {
            Enemy.StopAllMovementTasks();
            preparationForAttackTask = new Task(PreparationForAttackTask());
        }
    }

    private IEnumerator PreparationForAttackTask()
    {
        float time = 0f;

        while (time < SlipTime)
        {
            _agent.speed = _agent.speed * SlipForceDecreaseMultiplier;
            time += Time.deltaTime;
            yield return new WaitForFixedUpdate();
        }

        _agent.enabled = false;
        Rigidbody.velocity = Vector2.zero;
        Animator.SetTrigger("Prepare");
        PhysicsCollider.enabled = false;
    }
    
    public void EndPreparationState() // ����� ��� ������� ����� ��������
    {
        StateMachine.RunState(NextState);
    }
}
