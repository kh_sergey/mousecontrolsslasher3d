using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShieldActionState : EnemyActionState
{
    [SerializeField] private Collider2D ShieldTrigger;
    [SerializeField] private Collider2D DamageTrigger;

    protected override void Awake()
    {
        base.Awake();
    }

    public override void Run()
    {
        base.Run();
        Attack();
    }

    protected override void Attack()
    {
        if (attackTask == null || !attackTask.Running)
        {
            Enemy.StopAllMovementTasks();
            Animator.SetTrigger("Attack");
            ShieldTrigger.enabled = true;
        }
    }

    public override void ToggleDamageTrigger(int colliderStatus)
    {
        DamageTrigger.gameObject.SetActive(Convert.ToBoolean(colliderStatus));
    }

    public void ToggleShieldTrigger(int colliderStatus)
    {
        ShieldTrigger.enabled = Convert.ToBoolean(colliderStatus);
    }

    /*
    protected override void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.GetComponent<Player>() != null)
        {
            PlayerGameObject.GetComponent<Player>().Hit(AttackPower, KnockbackPower, transform.position);
        }
    }
    */

    public void EndActionState() // ��� ���������
    {
        StateMachine.RunState(NextState);
    }
}
