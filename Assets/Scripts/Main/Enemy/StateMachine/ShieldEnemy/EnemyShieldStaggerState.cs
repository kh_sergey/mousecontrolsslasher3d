using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShieldStaggerState : EnemyStaggerState
{
    [SerializeField] private GameObject _bodyGameObject;
    private bool weakStaggered = false;

    public override void Run()
    {
        base.Run();
        _agent.enabled = false;
        if (_hittedObject == _bodyGameObject)
        {
            StartStaggerAnimation();
            new Task(TakeDamage(_damageValue));
            //TakeDamage(_damageValue);
        } else
        {
            new Task(TakeDamage(0));
            //TakeDamage(0);
            StartWeakStaggerAnimation();
        }
        GetKnockbacked(_knockbackPointPosition, _receivedKnockbackPower);
    }

    private void FixedUpdate()
    {
        if (Running && weakStaggered)
        {
            RotateToTarget(PlayerGameObject.transform.position);
        }
    }

    private void GetKnockbacked(Vector3 knockbackPointPosition, float receivedKnockbackPower)
    {
        Enemy.StopAllMovementTasks();
        knockbackTask = new Task(KnockbackTask(receivedKnockbackPower, knockbackPointPosition));
    }

    protected override IEnumerator KnockbackTask(float receivedKnockbackPower, Vector3 knockbackPointPosition)
    {
        Vector3 directionFromStaggerPoint = (transform.position - knockbackPointPosition).normalized;
        Rigidbody.velocity = Vector3.zero; // ��� ��� � ������� ������ ��� ��������� ��������� �� ������������
        Rigidbody.velocity += new Vector2(directionFromStaggerPoint.x, directionFromStaggerPoint.y) * receivedKnockbackPower;

        while (Mathf.Abs(Rigidbody.velocity.x) + Mathf.Abs(Rigidbody.velocity.y) > 1f)
        {
            Rigidbody.velocity = Rigidbody.velocity * 0.95f;

            yield return new WaitForFixedUpdate();
        }

        Rigidbody.velocity = Vector3.zero;
    }

    protected override void StartStaggerAnimation()
    {
        Animator.SetTrigger("Staggered");
    }

    private void StartWeakStaggerAnimation()
    {
        Animator.SetTrigger("WeakStaggered");
        weakStaggered = true;
    }

    public void EndStaggerState()
    {
        weakStaggered = false;
        StateMachine.RunState(NextState);
    }
}