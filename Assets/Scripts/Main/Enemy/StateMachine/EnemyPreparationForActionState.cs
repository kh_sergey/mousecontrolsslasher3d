using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class EnemyPreparationForActionState : EnemyState
{
    [SerializeField] private float _slipTime;
    [SerializeField] private float _slipForceDecreaseMultiplier;

    public abstract void PreparationForAttack();

    public Task preparationForAttackTask { get; protected set; }
    public float SlipTime { get => _slipTime; private set => _slipTime = value; }
    public float SlipForceDecreaseMultiplier { get => _slipForceDecreaseMultiplier; private set => _slipForceDecreaseMultiplier = value; }

    protected override void Awake()
    {
        base.Awake();
        NextState = GetComponent<EnemyActionState>();
    }
}
