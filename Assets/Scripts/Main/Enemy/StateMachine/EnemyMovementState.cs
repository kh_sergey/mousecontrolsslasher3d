using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using UnityEngine.AI;

public abstract class EnemyMovementState : EnemyState
{
    [SerializeField] protected float _attackRange;
    //protected NavMeshAgent _agent;

    public float AttackRange { get => _attackRange; protected set => _attackRange = value; }

    protected override void Awake()
    {
        base.Awake();
        NextState = GetComponent<EnemyPreparationForActionState>();
        //_agent = GetComponent<NavMeshAgent>();
    }

    protected override void Start()
    {
        base.Start();
        //_agent.updateRotation = false;
        //_agent.updateUpAxis = false;
    }
}
