using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyStateMachine : MonoBehaviour
{
    private EnemyIdleState _idleState;
    private EnemyState _currentState;

    private void Awake()
    {
        _idleState = GetComponent<EnemyIdleState>();

        _currentState = _idleState;
        _currentState.Run();
    }

    public void RunState(EnemyState newState)
    {
        //if (isPaused == true)
        //{
            //Debug.Log("New state: " + newState);
            _currentState.Running = false;
            _currentState = newState;
            _currentState.Run();
        //}
    }

    public void RunIdleState()
    {
        _currentState.Running = false;
        _currentState = _idleState;
        _currentState.Run();
    }
}
