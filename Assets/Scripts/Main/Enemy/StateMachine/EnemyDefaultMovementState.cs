using System.Collections;
using System.Collections.Generic;
using UnityEngine.AI;
using UnityEngine;

public class EnemyDefaultMovementState : EnemyMovementState
{
    [SerializeField] private float _speed;
    [SerializeField] private LayerMask _raycastLayerMask;

    public float Speed { get => _speed; private set => _speed = value; }

    public override void Run()
    {
        base.Run();
        _agent.enabled = true;
    }
    
    private void Update()
    {
        if (Running)
        {
            _agent.SetDestination(PlayerGameObject.transform.position);
            RotateToTarget(new Vector3(Rigidbody.position.x, Rigidbody.position.y, 0) + _agent.velocity.normalized);
            CheckPlayerRange(AttackRange);
        }
    }

    private void CheckPlayerRange(float range)
    {
        if (PlayerGameObject.activeSelf)
        {
            if (Vector3.Distance(transform.position, PlayerGameObject.transform.position) <= range)
            {
                // �������� ��������� �� ���������� ������� � �������
                RaycastHit2D hit = Physics2D.Raycast(transform.position, (PlayerGameObject.transform.position - transform.position).normalized, range, _raycastLayerMask);
                Debug.DrawRay(transform.position, (PlayerGameObject.transform.position - transform.position).normalized * range, Color.red);

                if (hit.collider.tag != "Wall")
                {
                    StateMachine.RunState(NextState);
                }
            }
        }
    }
}
