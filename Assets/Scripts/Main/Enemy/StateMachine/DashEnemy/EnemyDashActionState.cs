using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class EnemyDashActionState : EnemyActionState
{
    [SerializeField] private float _attackDashForce;
    [SerializeField] private float _attackDashTime;
    [SerializeField] private Collider2D _attackCollider;
    [SerializeField] private Collider2D _physicsCollider;

    private float _agentSpeed;

    protected override void Awake()
    {
        base.Awake();
        _agentSpeed = _agent.speed;
    }

    protected override void Start()
    {
        base.Start();
        _attackCollider.enabled = false;
    }

    public override void Run()
    {
        base.Run();
        Attack();
    }

    protected override void Attack()
    {
        if (attackTask == null || !attackTask.Running)
        {
            Enemy.StopAllMovementTasks();
            attackTask = new Task(AttackTask());
            Animator.SetTrigger("Attack");
        }
    }

    private IEnumerator AttackTask()
    {
        float time = 0f;
        Vector2 attackDirection = (PlayerGameObject.transform.position - transform.position).normalized;
        Rigidbody.velocity = new Vector2(attackDirection.x, attackDirection.y).normalized * _attackDashForce;
        ToggleDamageTrigger(1);
        while (time < _attackDashTime)
        {
            time += Time.deltaTime;
            yield return new WaitForFixedUpdate();
        }
        ToggleDamageTrigger(0);
        Rigidbody.velocity = Vector2.zero;

        yield return new WaitForSeconds(0.2f);
        _agent.speed = _agentSpeed;
        if (PlayerGameObject.GetComponent<Player>().isActiveAndEnabled)
            StateMachine.RunState(NextState);
    }

    public override void ToggleDamageTrigger(int colliderStatus)
    {
        Physics2D.IgnoreCollision(_physicsCollider, PlayerGameObject.GetComponentInChildren<CircleCollider2D>(), Convert.ToBoolean(colliderStatus));
        _attackCollider.enabled = Convert.ToBoolean(colliderStatus);

        if (Convert.ToBoolean(colliderStatus))
        {
            new Task(DOTAnimations.TransitBodyColor(new Color(1f, 0.353f, 0f), 0.2f));
        }
        else
        {
            new Task(DOTAnimations.TransitBodyColor(DOTAnimations.OriginalColor, 0.2f));
        }
    }
}
