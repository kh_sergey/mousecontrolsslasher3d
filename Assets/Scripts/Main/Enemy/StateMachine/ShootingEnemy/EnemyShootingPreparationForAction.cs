using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShootingPreparationForAction : EnemyPreparationForActionState
{
    public override void Run()
    {
        base.Run();
        PreparationForAttack();
    }

    private void FixedUpdate()
    {
        if (Running)
        {
            RotateToTarget(PlayerGameObject.transform.position);
        }
    }

    public override void PreparationForAttack()
    {
        if (preparationForAttackTask == null || !preparationForAttackTask.Running)
        {
            Enemy.StopAllMovementTasks();
            preparationForAttackTask = new Task(PreparationForAttackTask());
        }
    }

    private IEnumerator PreparationForAttackTask()
    {
        float time = 0f;
        _agent.speed = 0f;

        //_agent.enabled = false;
        // ����� ������� ����� - ��������� ����������� ������. ���� ������ ���������� ��� ������ ����������. ��� �����:
        while (time < SlipTime)
        {
            //_agent.speed = _agent.speed * SlipForceDecreaseMultiplier;
            time += Time.deltaTime;
            yield return new WaitForFixedUpdate();
        }

        Animator.SetTrigger("Prepare");
    }

    public void EndPreparationState()
    {
        StateMachine.RunState(NextState);
    }
}
