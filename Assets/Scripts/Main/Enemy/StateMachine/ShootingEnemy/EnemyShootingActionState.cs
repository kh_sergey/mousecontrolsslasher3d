using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class EnemyShootingActionState : EnemyActionState
{
    [SerializeField] private Transform _firePoint;
    [SerializeField] private GameObject _enemyProjectile;
    [SerializeField] private AudioSource _projectileShootSound;

    private float _agentSpeed;

    protected override void Awake()
    {
        base.Awake();
        _agentSpeed = _agent.speed;
    }

    protected override void Start()
    {
        base.Start();
        //_attackCollider.enabled = false;
    }

    public override void Run()
    {
        base.Run();
        Attack();
    }

    protected override void Attack()
    {
        if (attackTask == null || !attackTask.Running)
        {
            Enemy.StopAllMovementTasks();
            attackTask = new Task(AttackTask());
            Animator.SetTrigger("Attack");
        }
    }

    private IEnumerator AttackTask()
    {
        // ������� ����, ����� ������� 0.2 �������
        Rigidbody.velocity = Vector2.zero;
        _agent.speed = 0f;

        GameObject spawnedProjectile = Instantiate(_enemyProjectile, _firePoint.position, _firePoint.rotation) as GameObject;
        Rigidbody2D projectileRigidbody = spawnedProjectile.GetComponent<Rigidbody2D>();
        projectileRigidbody.velocity = _firePoint.up * spawnedProjectile.GetComponent<EnemyProjectile>().ProjectileSpeed;

        yield return new WaitForSeconds(0.5f); // �� ���������� ������������. ����� ������������� ������.
        _agent.speed = _agentSpeed;
        if (PlayerGameObject.GetComponent<Player>().isActiveAndEnabled)
            StateMachine.RunState(NextState);
    }

    public override void ToggleDamageTrigger(int colliderStatus)
    {
        // �� �� �����. �� ��� ������� �������������� abstract �����, ��� ���� ����� � ������ Enemy ����� ���� ������� ��������� ���� ���������? ��� ����� �����
    }
}
