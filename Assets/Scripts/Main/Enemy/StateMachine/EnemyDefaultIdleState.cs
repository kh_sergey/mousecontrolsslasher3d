using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDefaultIdleState : EnemyIdleState
{
    [SerializeField] private LayerMask _raycastLayerMask;

    public override void Run()
    {
        base.Run();
    }

    private void FixedUpdate()
    {
        if (Running)
        {
            CheckPlayerRange(DetectionRange);
        }
    }

    private void CheckPlayerRange(float range)
    {
        if (Vector3.Distance(transform.position, PlayerGameObject.transform.position) <= range)
        {
            // �������� ��������� �� ���������� ������� � �������
            RaycastHit2D hit = Physics2D.Raycast(transform.position, (PlayerGameObject.transform.position - transform.position).normalized, range, _raycastLayerMask);
            Debug.DrawRay(transform.position, (PlayerGameObject.transform.position - transform.position).normalized * range, Color.red);

            if (hit.collider.GetComponentInParent<Player>() != null)
            {
                StateMachine.RunState(NextState);
            }
        }
    }
}
