using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyProjectile : MonoBehaviour
{
    [SerializeField] private float _projectileDamage;
    [SerializeField] private float _projectileKnockbackPower;
    [SerializeField] private float _projectileSpeed;

    private Rigidbody2D _rigidbody;

    public float ProjectileSpeed { get => _projectileSpeed; private set => _projectileSpeed = value; }

    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody2D>();
        _rigidbody.useFullKinematicContacts = true;
    }

    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.GetComponentInParent<Player>() != null)
        {
            collider.GetComponentInParent<Player>().Hit(_projectileDamage, _projectileKnockbackPower, transform.position);
            Destroy(gameObject);
        }

        if (collider.tag == "Wall")
        {
            
            Destroy(gameObject);
        }

        // ���� ��� �������� ������������� ����������� �� �������, �� ������ ���� ������� �� ������� ���� � �� ���������
    }
}
