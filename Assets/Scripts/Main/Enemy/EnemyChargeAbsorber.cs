using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyChargeAbsorber : MonoBehaviour
{
    [SerializeField] private ArenaController _arenaController;
    [SerializeField] private float _chargeAbsorptionAmount;

    private Enemy _enemy;
    private GameObject _playerGameObject;
    private bool absorptionEnabled = false;

    private void Awake()
    {
        _enemy = GetComponentInParent<Enemy>();
    }

    private void Start()
    {
        _playerGameObject = _enemy.PlayerSet.GetItemIndex(0);
    }

    private void Update()
    {
        if (_arenaController.CurrentArenaControllerRuntimeSet.GetItemIndex(0).GetComponent<ArenaController>() == _arenaController)
        {
            absorptionEnabled = true;
        }
        else
        {
            absorptionEnabled = false;
        }
    }

    private void FixedUpdate()
    {
        if (absorptionEnabled)
        {
            _playerGameObject.GetComponent<BounceCharge>().TryDecreaseValue(_chargeAbsorptionAmount);
        }
    }
}
