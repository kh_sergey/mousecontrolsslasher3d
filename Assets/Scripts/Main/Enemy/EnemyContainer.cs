using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyContainer : MonoBehaviour
{
    [HideInInspector] public ArenaController ArenaController;
    public GameObjectRuntimeSet EnemyRuntimeSet;

    void Start()
    {
        ArenaController = GetComponentInParent<ArenaController>();
    }
}
