using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyKeylogger : MonoBehaviour
{
    [SerializeField] private GameObject _leftButtonImagePrefab;
    [SerializeField] private GameObject _rightButtonImagePrefab;

    private List<GameObject> _readedButtons;
    private int _readedButtonsCount = 3;

    private void Awake()
    {
        // ������� �������� � ��������?
        _readedButtons = new List<GameObject>();
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            TryAddButtonToReadedButtons(_leftButtonImagePrefab);
        }

        if (Input.GetMouseButtonDown(1))
        {
            TryAddButtonToReadedButtons(_rightButtonImagePrefab);
        }

        for (int i = 1; i < _readedButtons.Count + 1; i++)
        {
            _readedButtons[i - 1].transform.position = new Vector3(gameObject.transform.position.x + (i * 2f) - 4f, gameObject.transform.position.y + 2f, gameObject.transform.position.z);
        }
    }

    private void TryAddButtonToReadedButtons(GameObject buttonToAdd)
    {
        GameObject imagePopup = Instantiate(buttonToAdd, new Vector3(transform.position.x, transform.position.y, transform.position.z), Quaternion.identity) as GameObject;
        imagePopup.transform.parent = gameObject.transform.parent;
        _readedButtons.Insert(0, imagePopup);
        if (_readedButtons.Count > _readedButtonsCount)
        {
            Destroy(_readedButtons[_readedButtons.Count - 1]);
            _readedButtons.RemoveAt(_readedButtons.Count - 1);
        }
    }

    public void DeleteAllReadedButtons()
    {
        foreach(GameObject readedButton in _readedButtons)
        {
            Destroy(readedButton);
        }
    }
}
