using System;
using System.Collections;
using UnityEngine;
using DG.Tweening;
using UnityEngine.AI;

public class Enemy: MonoBehaviour
{
    [SerializeField] private GameObject _takenDamagePopup;

    public EnemyHealth Health { get; private set; }
    public GameObjectRuntimeSet PlayerSet;

    private EnemyStateMachine _stateMachine;
    private EnemyStaggerState _staggerState;
    private NavMeshAgent _agent;
    private float _agentSpeed;
    private Vector3 _originalPosition;
    private Rigidbody2D _rigidbody;

    private void Awake()
    {
        Health = GetComponent<EnemyHealth>();
        _stateMachine = GetComponent<EnemyStateMachine>();
        _staggerState = GetComponent<EnemyStaggerState>();
        _agent = GetComponent<NavMeshAgent>();
        _agentSpeed = _agent.speed;
        _rigidbody = GetComponent<Rigidbody2D>();
        _originalPosition = transform.position;
    }

    public void Hit(float damageValue, float receivedKnockbackPower, Vector3 knockbackPointPosition, GameObject hittedObject)
    {
        _staggerState.Initialize(damageValue, receivedKnockbackPower, knockbackPointPosition, hittedObject);
        _stateMachine.RunState(_staggerState);
    }

    public void ShowDamagePopup(float takenDamage)
    {
        new Task(ShowDamagePopupTask(takenDamage));
    }

    private IEnumerator ShowDamagePopupTask(float takenDamage)
    {
        GameObject damagePopup = Instantiate(_takenDamagePopup, transform.position, Quaternion.identity) as GameObject;
        damagePopup.GetComponent<TextMesh>().text = takenDamage.ToString();
        yield return new WaitForSeconds(1f);
        Destroy(damagePopup);
    }

    public void Die()
    {
        if (TryGetComponent(out EnemyKeylogger keylogger))
        {
            keylogger.DeleteAllReadedButtons();
        }
        gameObject.SetActive(false);
    }

    public void Reload()
    {
        // ��������� �� ����� �������, ������������ ��, ��������� � ��������� ��������
        gameObject.SetActive(true);
        _agent.enabled = false;
        StopAllMovementTasks();
        Health.ResetHealth();
        _stateMachine.RunIdleState();
        _rigidbody.velocity = Vector3.zero;
        _rigidbody.angularVelocity = 0f;
        transform.position = _originalPosition;
        transform.rotation = Quaternion.Euler(0f, 0f, 0f);
    }

    public void StopAllMovementTasks()
    {
        GetComponent<EnemyActionState>().attackTask?.Stop();
        GetComponent<EnemyPreparationForActionState>().preparationForAttackTask?.Stop();
        GetComponent<EnemyStaggerState>().knockbackTask?.Stop();

        // ���� - �������� ������� (��� � �����)
        GetComponent<EnemyActionState>().ToggleDamageTrigger(0);
        _agent.speed = _agentSpeed;
        //_rigidbody.angularVelocity = 0f;
    }
}
