using System;
using System.Collections;
using UnityEngine;
using DG.Tweening;

public class EnemyHealth : MonoBehaviour
{
    private Enemy _enemy;
    private float _value;
    [SerializeField] private float _maxValue;

    public float MaxValue { get => _maxValue; private set => _maxValue = value; }
    public float Value { get => _value; private set => _value = value; }

    private void Awake()
    {
        _enemy = GetComponent<Enemy>();
    }

    private void Start()
    {
        Value = MaxValue;
    }

    public float GetHealthProcent()
    {
        return Value / MaxValue;
    }

    public void ResetHealth()
    {
        Value = MaxValue;
    }

    public void Damage(float damageValue)
    {
        Value -= damageValue;
        if (Value < 0)
        {
            Value = 0;
        }

        if (Value == 0)
        {
            _enemy.Die();
        }
    }

    public void Heal(float healValue)
    {
        Value += healValue;
        if (Value > MaxValue) Value = MaxValue;
    }
}
