using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;
using TMPro;
using System;

public class SettingsMenu : MonoBehaviour
{
    [SerializeField] private TMP_Dropdown _resolutionDropdown;
    [SerializeField] private TMP_Dropdown _graphicsDropdown;
    [SerializeField] private TMP_Dropdown _controlsDropdown;
    [SerializeField] private Slider _audioSlider;
    [SerializeField] private AudioMixer _audioMixer;
    private Resolution[] _resolutions;
    private float _volumeValue;
    private int _graphicsQualityIndexValue;
    private int _resolutionIndexValue;
    private int _controlsIndexValue;

    private void Start()
    {
        float volumeValue = PlayerPrefs.GetFloat("VolumeValue", _volumeValue);
        _audioMixer.SetFloat("volume", volumeValue);
    }

    public void InitializeSettings()
    {
        InitializeResolutionSetting();
        InitializeAudioSetting();
        InitializeGraphicsSetting();
        InitializeControlsSetting();
    }

    private void InitializeResolutionSetting()
    {
        _resolutions = Screen.resolutions;

        _resolutionDropdown.ClearOptions();

        List<string> options = new List<string>();

        int currentResolutionIndex = 0;
        for (int i = 0; i < _resolutions.Length; i++)
        {
            string option = _resolutions[i].width + " x " + _resolutions[i].height;
            options.Add(option);

            if (_resolutions[i].width == Screen.currentResolution.width &&
                _resolutions[i].height == Screen.currentResolution.height)
            {
                currentResolutionIndex = i;
            }
        }

        _resolutionDropdown.AddOptions(options);
        _resolutionDropdown.value = currentResolutionIndex;
        _resolutionDropdown.RefreshShownValue();
    }

    private void InitializeAudioSetting()
    {
        _audioMixer.GetFloat("volume", out float audioValue);
        _audioSlider.value = audioValue;
    }

    private void InitializeGraphicsSetting()
    {
        int qualityLevelIndex = QualitySettings.GetQualityLevel();
        _graphicsDropdown.value = qualityLevelIndex;
    }

    private void InitializeControlsSetting()
    {
        _controlsDropdown.value = PlayerPrefs.GetInt("ControlsScheme");
    }

    public void SetVolumeValue(float volume)
    {
        _volumeValue = volume;
    }

    public void SetQualityValue(int qualityIndex)
    {
        _graphicsQualityIndexValue = qualityIndex;
    }

    public void SetResolutionValue(int resolutionIndex)
    {
        _resolutionIndexValue = resolutionIndex;
    }

    public void SetControlsValue(int controlsIndex)
    {
        _controlsIndexValue = controlsIndex;
    }

    private void SetVolume(float volume)
    {
        _audioMixer.SetFloat("volume", volume);
        PlayerPrefs.SetFloat("VolumeValue", _volumeValue);
    }

    private void SetQuality(int qualityIndex)
    {
        QualitySettings.SetQualityLevel(qualityIndex);
        PlayerPrefs.SetInt("GraphicsQualityIndex", _graphicsQualityIndexValue);
    }

    private void SetResolution(int resolutionIndex)
    {
        Resolution resolution = _resolutions[resolutionIndex];
        Screen.SetResolution(resolution.width, resolution.height, Screen.fullScreen);
        PlayerPrefs.SetString("Resolution", resolution.width + " x " + resolution.height);
    }

    private void SetControls(int controlsIndex)
    {
        PlayerPrefs.SetInt("ControlsScheme", _controlsIndexValue);
    }

    public void SaveSettings()
    {
        SetVolume(_volumeValue);
        SetQuality(_graphicsQualityIndexValue);
        SetResolution(_resolutionIndexValue);
        SetControls(_controlsIndexValue);
    }
}
