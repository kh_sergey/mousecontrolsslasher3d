using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;

public class GameManager : MonoBehaviour
{
    public bool IsPaused { get; private set; } = false;

    [SerializeField] private string _mainMenuScene;
    [SerializeField] private string _nextLevelScene;
    [SerializeField] private bool _toggleDebug;
    [SerializeField] private GameObject _debugArenaToLoad;
    [SerializeField] private int _debugToggleCutscene;

    private bool started = false; // � ����� ��������� ������������ ������? ��� �� ������� ������� ������� ����� 1 ��� � FixedUpdate'e 

    private void FixedUpdate()
    {
        // �� �������� ��������� ����� �� ��������� ���� ����� ������ (Start ���������� ����� ����������� ������� ������, � ��� ���������� �������������)
        // ���� ��������� ����� � Start - ���� ����������� ��������� � ��� �� ������������������� �������.

        if (!started)
        {
            started = true;

            LoadArenaFromPlayerPrefs();
        }
    }

    private void LoadArenaFromPlayerPrefs()
    {
        started = true;

        if (_toggleDebug)
        {
            PlayerPrefs.SetInt("PlayCutcsene", _debugToggleCutscene);
            PlayerPrefs.SetString("Arena", _debugArenaToLoad.name);
        }

        GameObject.Find(PlayerPrefs.GetString("Arena")).GetComponent<ArenaController>().LoadArena(); // ������ �� ����� ����� �����, �� � PlayerPrefs ���� ������ �������� ������??
    }

    public void TogglePause(bool toggle)
    {
        IsPaused = toggle;
    }

    public void ToggleTimeScale(bool toggle)
    {
        if (toggle)
        {
            Time.timeScale = 1;
        } else
        {
            Time.timeScale = 0;
        }
    }

    public void ExitToMainMenu()
    {
        SceneManager.LoadScene(_mainMenuScene);
    }

    public void ReloadScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void CompleteLevel()
    {
        /*
        if (!String.IsNullOrEmpty(_nextLevelScene))
        {
            PlayerPrefs.SetInt("MaxAchievedLevel", Convert.ToInt32(_nextLevelScene.Remove(0, 5)));
        }
        */
    }

    public void LoadNextLevel()
    {
        PlayerPrefs.SetString("Arena", "Arena1");
        PlayerPrefs.SetInt("PlayCutcsene", 1);
        SceneManager.LoadScene(_nextLevelScene);
    }
}
