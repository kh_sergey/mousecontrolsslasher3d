using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class RuntimeSet<T> : ScriptableObject
{
    private List<T> items = new List<T>();

    public void Initialize()
    {
        items.Clear();
    }

    public T GetItemIndex(int index)
    {
        return items[index];
    }

    public int GetItemsCount()
    {
        return items.Count;
    }

    public void AddToList(T objectToAdd)
    {
        if (!items.Contains(objectToAdd))
        {
            items.Add(objectToAdd);
        }
    }

    public void RemoveFromList(T objectToRemove)
    {
        if (items.Contains(objectToRemove))
        {
            items.Remove(objectToRemove);
        }
    }
}
