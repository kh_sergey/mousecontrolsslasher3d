using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddGameObjectToRuntimeSet : MonoBehaviour
{
    public GameObjectRuntimeSet GameObjectRuntimeSet;

    private void OnEnable()
    {
        GameObjectRuntimeSet.AddToList(this.gameObject);
    }

    private void OnDisable()
    {
        GameObjectRuntimeSet.RemoveFromList(this.gameObject);
    }
}
