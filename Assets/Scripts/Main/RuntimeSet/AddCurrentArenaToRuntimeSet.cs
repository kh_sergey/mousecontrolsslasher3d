using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddCurrentArenaToRuntimeSet : MonoBehaviour
{
    public GameObjectRuntimeSet GameObjectRuntimeSet;

    private void OnEnable()
    {
        GameObjectRuntimeSet.AddToList(this.gameObject);
    }

    private void OnDisable()
    {
        GameObjectRuntimeSet.Initialize();
    }
}
