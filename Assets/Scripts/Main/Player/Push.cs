using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Push : MonoBehaviour
{
    // ������ ������ ������.
    [SerializeField] private float pushDamageValue;
    [SerializeField] private float pushKnockbackPower;
    [SerializeField] private float absoluteVelocityForPush;
    private PlayerMovement _playerMovement;
    private Rigidbody2D _rigidbody;
    private PlayerAttack _playerAttack;
    private Player _player;

    private void Awake()
    {
        _player = GetComponent<Player>();
        _playerMovement = GetComponent<PlayerMovement>();
        _playerAttack = GetComponentInChildren<PlayerAttack>();
        _rigidbody = GetComponent<Rigidbody2D>();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        // ���� �� �������� � �� ���������� ����������� �����
        if (Mathf.Abs(_rigidbody.velocity.x) + Mathf.Abs(_rigidbody.velocity.y) >= absoluteVelocityForPush && !_playerAttack.UltimativeAttack && _player.ActionPossibility)
        {
            if (collision.collider.tag == "Enemy" || collision.collider.tag == "EnemyShield") // �������� ������ �� ��� ������ ��� ����� ���� ������������ �� ����� ��� ����� ������ �������� ����������
            {                                           // ������� �� ����� ����� ���������� ����������
                collision.collider.GetComponentInParent<Enemy>().Hit(pushDamageValue, pushKnockbackPower, transform.position, collision.collider.gameObject);
                // ����� ������� ������ ��������� Enemy. ���� ���� ������ � ���, �� ����� ������ Enemy � ����.
            }
        }
    }
}
