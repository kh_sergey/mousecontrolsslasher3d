using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerArenaInteraction : MonoBehaviour
{
    [SerializeField] private GameObjectRuntimeSet _currentArenaControllerRuntimeSet;

    public void TryPlayCutscene()
    {
        _currentArenaControllerRuntimeSet.GetItemIndex(0).GetComponent<ArenaController>().TryPlayCutscene();
    }
}
