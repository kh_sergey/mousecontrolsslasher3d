using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class PlayerUI : MonoBehaviour
{
    //[SerializeField] private Text _textHealth;
    //[SerializeField] private Text _textCharges;
    //[SerializeField] private Text _textArrows;

    [SerializeField] private Slider _healthSlider;
    [SerializeField] private Slider _chargeSlider;
    [SerializeField] private Slider _projectilesSlider;
    [SerializeField] private GameObject _forceParent;

    private Vector3 _forceParentStartPosition;
    private PlayerInput _playerInput;
    private PlayerHealth _health;
    private BounceCharge _bounceCharge;
    private PlayerProjectilesCapacity _projectilesCapacity;

    public SpriteMask ForceSpriteMask { get; private set; }
    public SpriteRenderer ForceBackground { get; private set; }
    public GameObject ForceParent { get => _forceParent; private set => _forceParent = value; }

    private void Awake()
    {
        _forceParentStartPosition = _forceParent.transform.position;
        ForceSpriteMask = _forceParent.GetComponent<SpriteMask>();
        _playerInput = GetComponent<PlayerInput>();
        _health = GetComponent<PlayerHealth>();
        _bounceCharge = GetComponent<BounceCharge>();
        _projectilesCapacity = GetComponent<PlayerProjectilesCapacity>();

        HideForce();
    }

    private void Start()
    {
        _healthSlider.maxValue = _health.MaxValue;
        _chargeSlider.maxValue = _bounceCharge.MaxValue;
        _projectilesSlider.maxValue = _projectilesCapacity.MaxValue;

        UpdateCanvasInfo();
    }

    private void Update()
    {
        // ��� ������� �������
        _forceParent.transform.position = new Vector3(transform.position.x + _forceParentStartPosition.x, transform.position.y + _forceParentStartPosition.y, _forceParentStartPosition.z);
    }

    public void ShowForce(float holdTime)
    {
        _forceParent.SetActive(true);
        ForceSpriteMask.alphaCutoff = 1 - (holdTime / _playerInput.MaxHoldTime);
    }

    public void HideForce()
    {
        _forceParent.SetActive(false);
    }

    public void UpdateCanvasInfo()
    {
        //_textHealth.text = "Health: " + _health.Value;
        //_textCharges.text = "Charges: " + _bounceCharge.Value;
        //_textArrows.text = "Arrows: " + _projectilesCapacity.Value;

        _healthSlider.value = _health.Value;
        _chargeSlider.value = _bounceCharge.Value;
        _projectilesSlider.value = _projectilesCapacity.Value;
    }
}
