using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.Events;

public class SecondaryShootAnimation : MonoBehaviour
{
    [SerializeField] private Transform _primaryTransform;
    [SerializeField] private Transform _secondaryTransform;
    [SerializeField] private float _animatorDefaultSpeed;
    [SerializeField] private float _tick;

    private PlayerShoot _playerShoot;
    private Sequence _sequence;
    private Animator _animator;

    public Animator Animator { get => _animator; private set => _animator = value; }

    private void Awake()
    {
        _playerShoot = GetComponent<PlayerShoot>();
        _animator = GetComponentInParent<Animator>();

        Animator.speed = _animatorDefaultSpeed;
    }

    public void PlayAiming()
    {
        new Task(PlayAimingTask());
    }

    public IEnumerator PlayAimingTask()
    {
        _sequence.Kill();
        _animator.enabled = false;    // ���� �� ������� �������� ������?
        _sequence = DOTween.Sequence();

        _sequence.Append(_secondaryTransform.transform.DOLocalMove(new Vector2(-1.005f, 0.501f), _tick * 4)); // ����������� �� �� ������������� �����, � � �����������
        _sequence.Insert(0, _secondaryTransform.transform.DOLocalRotate(new Vector3(0, 0, -110.66f), _tick * 4)); // �� ��������� �������
        _sequence.Insert(0, _primaryTransform.transform.DOLocalMove(new Vector2(0.83f, -0.81f), _tick * 4));
        _sequence.Insert(0, _primaryTransform.transform.DOLocalRotate(new Vector3(0, 0, -34.46f), _tick * 4));

        _sequence.Append(_secondaryTransform.transform.DOLocalMove(new Vector2(-1.005f, 0.501f), _tick * 1));
        _sequence.Join(_secondaryTransform.transform.DOLocalRotate(new Vector3(0, 0, -110.66f), _tick * 1));
        _sequence.Join(_primaryTransform.transform.DOLocalMove(new Vector2(0.83f, -0.81f), _tick * 1));
        _sequence.Join(_primaryTransform.transform.DOLocalRotate(new Vector3(0, 0, -34.46f), _tick * 1));

        _sequence.AppendCallback(() => _playerShoot.TryShoot());

        _sequence.Append(_secondaryTransform.transform.DOLocalMove(new Vector2(-1.005f, 0.501f), _tick * 10)); // �� ���� ��� �������
        _sequence.Join(_secondaryTransform.transform.DOLocalRotate(new Vector3(0, 0, -110.66f), _tick * 10));
        _sequence.Join(_primaryTransform.transform.DOLocalMove(new Vector2(0.83f, -0.81f), _tick * 10));
        _sequence.Join(_primaryTransform.transform.DOLocalRotate(new Vector3(0, 0, -34.46f), _tick * 10));

        _sequence.AppendCallback(() => { _playerShoot.ToggleProjectileDummyActivity(true); _playerShoot.SetActionPossibility(true); });

        _sequence.OnComplete(() => { PlayLoweringWeapon(); });

        yield return new WaitForFixedUpdate();
    }

    public void PlayLoweringWeapon()
    {
        _sequence.Kill();
        _animator.enabled = false;    // ���� �� ������� �������� ������?
        _sequence = DOTween.Sequence();

        _sequence.Append(_secondaryTransform.transform.DOLocalMove(new Vector2(-1.25f, -0.15f), _tick * 9));
        _sequence.Insert(0, _secondaryTransform.transform.DOLocalRotate(new Vector3(0, 0, 10f), _tick * 9));
        _sequence.Insert(0, _primaryTransform.transform.DOLocalMove(new Vector2(1.18f, 0f), _tick * 9));
        _sequence.Insert(0, _primaryTransform.transform.DOLocalRotate(new Vector3(0, 0, -12f), _tick * 9));

        _sequence.OnComplete(() => { _animator.enabled = true; });
    }
}
