using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShoot : MonoBehaviour
{
    // ������� �������� ���. ������� ��������, ����� ������� ���������� (�������� �� ���������� �����)
    // ���� ������� (���� ������) ��������� ������. ������ ���� ���� �� ����������� �������� (��� ������������ ��� �� �� PlayerAttack, ActionPossibility)
    // ������� ������ ���������������� ��� ���� � ����������� ��� ������ ��������.

    [SerializeField] private AudioSource _projectileShootSound;
    [SerializeField] private AudioSource _projectileClickSound;
    [SerializeField] private GameObject _defaultProjectile;
    [SerializeField] private GameObject _chargedProjectile;
    [SerializeField] private Transform _firePoint;

    private Player _player;
    private PlayerProjectilesCapacity _projectilesCapacity;
    private BounceCharge _bounceCharge;
    private SecondaryShootAnimation _shootAnimation;

    public bool Shoots { get; private set; } = false;
    public GameObject DefaultProjectile { get => _defaultProjectile; private set => _defaultProjectile = value;  }
    public GameObject ChargedProjectile { get => _chargedProjectile; private set => _chargedProjectile = value; }

    private void Awake()
    {
        _player = GetComponentInParent<Player>();
        _shootAnimation = GetComponent<SecondaryShootAnimation>();
        _projectilesCapacity = GetComponentInParent<PlayerProjectilesCapacity>();
        _bounceCharge = GetComponentInParent<BounceCharge>();
    }

    public void Shoot()
    {
        _player.ActionPossibility = false;
        _shootAnimation.PlayAiming();
    }

    public void TryShoot()
    {
        bool shootPossibility = _projectilesCapacity.TryDecreaseValue();

        if (shootPossibility)
        {
            _projectileShootSound.Play();
            GameObject projectileGameObject;

            if (_bounceCharge.TryDecreaseValue(0.15f))
            {
                // ������� ��������� ������
                projectileGameObject = _chargedProjectile;
            } else
            {
                // ������� ������� ������
                projectileGameObject = _defaultProjectile;
            }

            // ����� ���� ��� ������� ������, ����� ������ - �������� � �������. ������ ������ ������? �� ������, ��� �������
            projectileGameObject = Instantiate(projectileGameObject, _firePoint.position, _firePoint.rotation) as GameObject;
            Rigidbody2D projectileRigidbody = projectileGameObject.GetComponent<Rigidbody2D>();
            projectileRigidbody.velocity = _firePoint.up * projectileGameObject.GetComponent<PlayerProjectile>().ProjectileSpeed;
            ToggleProjectileDummyActivity(false);
        } else
        {
            _projectileClickSound.Play();
        }
    }

    public void ToggleProjectileDummyActivity(bool state)
    {
        if (_projectilesCapacity.Value > 0)
        {
            _bounceCharge.CurrentProjectileDummy.SetActive(state);
        } else
        {
            _bounceCharge.CurrentProjectileDummy.SetActive(false);
        }
    }

    public void SetActionPossibility(bool possibility)
    {
        _player.ActionPossibility = possibility;
    }
}
