using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    // �������� ��������� ������������ - ��� ������ ��������, ��� ������ � ������� ����������� �����.

    [SerializeField] private float _attackDashForce;
    [SerializeField] private float _attackDashTime;
    [SerializeField] private float _slipTime;
    [SerializeField] private float _slipForceStopValue;
    [SerializeField] private float _slipForceDecreaseMultiplier;
    [SerializeField] private float _ultimativeDashForce;
    [SerializeField] private float _maxMoveTime;
    [SerializeField] private float _absorbForUltimativeAttack;
    [SerializeField] private float[] _minMaxLongDashDistances; // ������ � ������� �������� ����������� � ������������ ���������� �����������
    [SerializeField] private CircleCollider2D _physicsCollider;

    private Player _player;
    private PlayerAttack _playerAttack;
    private PlayerInput _playerInput;
    private TrailRenderer _trailRenderer;
    private Rigidbody2D _rigidbody;
    private BounceCharge _bounceCharge;
    private PlayerUI _playerUI;

    public bool stopUltimativeDash { get; set; } = false;
    public Task attackDashTask { get; private set; }
    public Task longDashTask { get; private set; }
    public Task dashToMouseTask { get; private set; }
    public PhysicsMaterial2D PhysicsMaterial { get; private set; }

    public float AttackDashForce { get => _attackDashForce; private set => _attackDashForce = value; }
    public float AttackDashTime { get => _attackDashTime; private set => _attackDashTime = value; }

    public enum MovementStates { None, Pushing, Sliding };
    private MovementStates _movementState;
    public MovementStates MovementState
    {
        get => _movementState; set
        {
            _movementState = value;
            switch (_movementState)
            {
                case MovementStates.Pushing:
                    _trailRenderer.startColor = Color.red;
                    break;
                case MovementStates.Sliding:
                    _trailRenderer.startColor = Color.blue;
                    break;
                case MovementStates.None:
                    _trailRenderer.startColor = Color.white;
                    break;
            }
        }
    }

    private void Awake()
    {
        _player = GetComponent<Player>();
        _playerAttack = GetComponentInChildren<PlayerAttack>();
        _playerInput = GetComponent<PlayerInput>();
        _trailRenderer = GetComponent<TrailRenderer>();
        _rigidbody = GetComponent<Rigidbody2D>();
        _bounceCharge = GetComponent<BounceCharge>();
        _playerUI = GetComponent<PlayerUI>();

        PhysicsMaterial = new PhysicsMaterial2D();
        MovementState = MovementStates.None;
    }

    private void Start()
    {
        ChangeBounciness(0.8f);
    }

    public void RotateToPosition(Vector3 position)
    {
        Vector3 direction = new Vector3(position.x - transform.position.x, position.y - transform.position.y);
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        Quaternion rotation = Quaternion.Euler(new Vector3(angle, -90, -90));

        if (_playerAttack.AttackSlowdown)
            transform.rotation = Quaternion.Slerp(transform.rotation, rotation, 0.5f * Time.deltaTime);
        else
            transform.rotation = Quaternion.Slerp(transform.rotation, rotation, 10f * Time.deltaTime);
    }

    public void StartUltimativeDash()
    {
        _player.StopAllMovementTasks();
        dashToMouseTask = new Task(UltimativeDashToMousePosition());
    }
    
    public void StopUltimativeDash()
    {
        stopUltimativeDash = true;
    }

    private IEnumerator UltimativeDashToMousePosition()
    {
        ChangeBounciness(0f);

        while (!stopUltimativeDash)
        {
            var direction = (Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position);
            Vector3 screenToWorldPointWithoutZ = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            screenToWorldPointWithoutZ.z = transform.position.z;

            if (Vector3.Distance(screenToWorldPointWithoutZ, transform.position) > 0.5f)
            {
                _rigidbody.velocity = new Vector3(direction.x, direction.y).normalized * _ultimativeDashForce;
            } else
            {
                _rigidbody.velocity = Vector3.zero;
            }

            _bounceCharge.TryDecreaseValue(_absorbForUltimativeAttack);

            yield return new WaitForFixedUpdate();
        }

        stopUltimativeDash = false;
        ChangeBounciness(0.8f);

        while (Mathf.Abs(_rigidbody.velocity.x) + Mathf.Abs(_rigidbody.velocity.y) > 1f)
        {
            _rigidbody.velocity = _rigidbody.velocity * 0.96f;
            yield return new WaitForFixedUpdate();
        }

        _rigidbody.velocity = Vector3.zero;
    }

    public void LongDashToPosition(float dashHoldTime, Vector3 dashPoint)
    {
        _player.StopAllMovementTasks();
        longDashTask = new Task(LongDashToPositionTask(dashHoldTime, dashPoint));
    }

    private IEnumerator LongDashToPositionTask(float dashHoldTime, Vector3 dashPoint)
    {
        float time = 0f;
        float distanceTravelled = 0f;
        float dashDistance = CalculateDashDistance(dashHoldTime);
        float dashForce = _maxMoveTime * dashDistance;
        float slipForce = dashForce;

        Vector2 lastPosition = transform.position;
        Vector2 dashDirection = (dashPoint - transform.position);
        dashDirection = new Vector2(dashDirection.x, dashDirection.y).normalized;

        _rigidbody.velocity = dashDirection * dashForce;

        MovementState = MovementStates.Pushing;
        _playerUI.HideForce();

        while (distanceTravelled < dashDistance && time < _maxMoveTime)
        {
            distanceTravelled += Vector2.Distance(lastPosition, transform.position);
            lastPosition = transform.position;
            time += Time.deltaTime;
            yield return new WaitForFixedUpdate();
        }

        time = 0f;
        MovementState = MovementStates.Sliding;
        while (time < _slipTime && slipForce > _slipForceStopValue)
        {
            _rigidbody.velocity = _rigidbody.velocity * _slipForceDecreaseMultiplier;
            time += Time.deltaTime;
            yield return new WaitForFixedUpdate();
        }

        _rigidbody.velocity = Vector2.zero;
        MovementState = MovementStates.None;

    }

    public void AttackDash(Vector3 attackPoint)
    {
        _player.StopAllMovementTasks();
        attackDashTask = new Task(AttackDashTask(attackPoint)); 
    }

    private IEnumerator AttackDashTask(Vector3 attackPoint)
    {
        float time = 0f;

        ChangeBounciness(0f);

        Vector3 attackDashDirection = (attackPoint - transform.position).normalized;
        if (Mathf.Abs(_rigidbody.velocity.x) + Mathf.Abs(_rigidbody.velocity.y) < 10f)
        {
            _rigidbody.velocity = new Vector3(attackDashDirection.x, attackDashDirection.y).normalized * AttackDashForce;
        }
        else
        {
            _rigidbody.velocity = new Vector3(attackDashDirection.x, attackDashDirection.y).normalized * (Mathf.Abs(_rigidbody.velocity.x) + Mathf.Abs(_rigidbody.velocity.y));
        }

        while (time < AttackDashTime)
        {
            time += Time.deltaTime;
            yield return new WaitForFixedUpdate();
        }

        ChangeBounciness(0.8f);

        _rigidbody.velocity = Vector3.zero;
    }

    private float CalculateDashDistance(float launchHoldTime)
    {
        float launchDistance;
        float[] distances = new float[2] { _minMaxLongDashDistances[0], _minMaxLongDashDistances[1] };

        float tmpValue; // ����� ���������� ������� ����� ���� � ����������� ��������� �������
        float procentTime = (launchHoldTime / _playerInput.MaxHoldTime);

        tmpValue = distances[0];
        distances[0] -= tmpValue;
        distances[1] -= tmpValue;
        launchDistance = (distances[1] * procentTime) + tmpValue;

        return launchDistance;
    }

    public void ChangeBounciness(float bounciness)
    {
        PhysicsMaterial.bounciness = bounciness;
        PhysicsMaterial.friction = 0f;
        _physicsCollider.sharedMaterial = PhysicsMaterial;
    }

}
