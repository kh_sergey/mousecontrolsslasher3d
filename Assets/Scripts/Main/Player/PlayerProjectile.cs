using System.Collections;
using System.Collections.Generic;
using Unity;
using UnityEngine;

public class PlayerProjectile : MonoBehaviour
{
    [SerializeField] private float _projectileDamage;
    [SerializeField] private float _projectileKnockbackPower;
    [SerializeField] private float _projectileSpeed;
    [SerializeField] private int _maxBounceCount;

    private int _bounceCount = 0;
    private Rigidbody2D _rigidbody;

    public float ProjectileSpeed { get => _projectileSpeed; private set => _projectileSpeed = value; }
    
    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody2D>();
        _rigidbody.useFullKinematicContacts = true;
    }

    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.GetComponentInParent<Enemy>() != null)
        {
            collider.GetComponentInParent<Enemy>().Hit(_projectileDamage, _projectileKnockbackPower, transform.position, collider.gameObject);
            Destroy(gameObject);
        }

        if (collider.tag == "EnemyShield")
        {
            Destroy(gameObject);
        }

        //Destroy(gameObject);

        // ���� ��� �������� ������������� ����������� �� �������, �� ������ ���� ������� �� ������� ���� � �� ���������
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "Wall")
        {
            if (_bounceCount >= _maxBounceCount)
            {
                Destroy(gameObject);
            }

            Vector2 reflectedDirection = Vector2.Reflect(_rigidbody.velocity, collision.contacts[0].normal);
            transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y, -transform.rotation.eulerAngles.z);
            _rigidbody.velocity = reflectedDirection;

            _bounceCount++;
        }
    }
}
