using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    [SerializeField] private GameObjectRuntimeSet _cameraControllerRuntimeSet;

    public Color OriginalColor { get; private set; }
    public Vector3 OriginalScale { get; private set; }

    private CameraController _cameraController;
    private PlayerAttack _playerAttack;
    private PrimaryAttackAnimation _attackAnimation;
    private PlayerMovement _playerMovement;
    private PlayerDamageReaction _playerDamageReaction;
    private PlayerUI _playerUI;
    private PlayerShoot _playerShoot;
    private Rigidbody2D _rigidbody;

    public bool ActionPossibility { get; set; } = true;

    public GameObjectRuntimeSet EnemySet;

    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody2D>();
        _playerAttack = GetComponentInChildren<PlayerAttack>();
        _attackAnimation = GetComponentInChildren<PrimaryAttackAnimation>();
        _playerMovement = GetComponent<PlayerMovement>();
        _playerDamageReaction = GetComponent<PlayerDamageReaction>();
        _playerUI = GetComponent<PlayerUI>();
        _playerShoot = GetComponentInChildren<PlayerShoot>();

        OriginalColor = GetComponentInChildren<MeshRenderer>().material.color;
        OriginalScale = transform.localScale;
    }

    private void Start()
    {
        _cameraController = _cameraControllerRuntimeSet.GetItemIndex(0).GetComponent<CameraController>();
    }

    public void LongDashToPosition(float dashHoldTime, Vector3 dashPoint)
    {
        _playerMovement.LongDashToPosition(dashHoldTime, dashPoint);
    }

    public void Attack()
    {
        _playerAttack.Attack();
        _playerMovement.AttackDash(Camera.main.ScreenToWorldPoint(Input.mousePosition));
    }

    public void Shoot()
    {
        _playerShoot.Shoot();
    }

    public void StartUltimativeAttack()
    {
        _playerAttack.StartUltimativeAttack();
        _playerMovement.StartUltimativeDash();
    }

    public void ExecuteUltimativeAttack()
    {
        if (_playerAttack.UltimativeAttack)
        {
            _playerAttack.ExecuteUltimativeAttack();
            _playerMovement.StopUltimativeDash();
        }
    }

    public void Hit(float attackPower, float knockbackPower, Vector3 knockbackPointPosition)
    {
        _playerDamageReaction.Hit(attackPower, knockbackPower, knockbackPointPosition);
    }

    public void Die()
    {
        Debug.Log("Player died");
        gameObject.SetActive(false);
        _playerUI.ForceParent.SetActive(false);
        // Идёт анимация FadeIn на месте, потом враги и игрок становятся на стандартные позиции и FadeOut
        _cameraController.PlayFadeInDeathAnimation();
    }

    public void StopAllMovementTasks()
    {
        _playerDamageReaction.knockbackTask?.Stop();
        _playerMovement.longDashTask?.Stop();
        _playerMovement.attackDashTask?.Stop();
        _playerMovement.dashToMouseTask?.Stop();

        // На случай если была прервана корутина дэша. Костыль. Худший в проекте. Очень легко что-то упустить.
        _playerMovement.stopUltimativeDash = false;
        _playerMovement.MovementState = PlayerMovement.MovementStates.None;
        _rigidbody.velocity = Vector2.zero;
        _playerMovement.ChangeBounciness(0.8f);
    }

    public void ToggleWeaponCollider(int colliderState) // Двойной костыль. Нельзя использовать bool
    {                                                   // + нельзя использовать функцию из PlayerAttack т.к. доступны 
        if (colliderState == 0)                         // только методы из скриптов с того же объекта
        {
            _playerAttack.ToggleWeaponCollider(false);
        }
        else if (colliderState == 1)
        {
            _playerAttack.ToggleWeaponCollider(true);
        }
        else
            throw new ArgumentException();
    }

    public void ChangeAttackStatusToNone() // Тот же костыль
    {
        _attackAnimation.TogglePrimaryAnimationStatus(PrimaryAttackAnimation.AttackStates.None);
    }
}