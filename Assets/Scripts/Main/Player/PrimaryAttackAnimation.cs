using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.Events;

public class PrimaryAttackAnimation : MonoBehaviour
{
    [SerializeField] private Transform _primaryTransform;
    [SerializeField] private Transform _secondaryTransform;
    [SerializeField] private float _animatorDefaultSpeed;
    [SerializeField] private float _animatorChargedSpeed;

    private Animator _animator;

    public enum AttackStates { None, FirstAttack, SecondAttack, ChargeSwing }
    public AttackStates AttackState { get; private set; }
    public Animator Animator { get => _animator; private set => _animator = value; }
    public float AnimatorChargedSpeed { get => _animatorChargedSpeed; private set => _animatorChargedSpeed = value; }
    public float AnimatorDefaultSpeed { get => _animatorDefaultSpeed; private set => _animatorDefaultSpeed = value; }

    private Sequence sequence;

    private void Awake()
    {
        AttackState = AttackStates.None;
        _animator = GetComponentInParent<Animator>();

        Animator.speed = _animatorDefaultSpeed;
    }

    public void TogglePrimaryAnimationStatus(PrimaryAttackAnimation.AttackStates newState)
    {
        AttackState = newState;
    }

    public void PlayFirstAttack(float tick)
    {
        sequence.Kill();
        _animator.enabled = false;    // ���� �� ������� �������� ������?
        sequence = DOTween.Sequence();
        AttackState = AttackStates.FirstAttack;

        sequence.Append(_secondaryTransform.transform.DOLocalMove(new Vector2(-1.2303f, 0.611f), tick * 9));
        sequence.Insert(0, _secondaryTransform.transform.DOLocalRotate(new Vector3(0, 0, -33.34f), tick * 9));
        sequence.Insert(0, _primaryTransform.transform.DOLocalMove(new Vector2(0.82813f, -0.58f), tick * 9));
        sequence.Insert(0, _primaryTransform.transform.DOLocalRotate(new Vector3(0, 0, -137.27f), tick * 9));

        sequence.OnComplete(() => { _animator.enabled = true; _animator.SetTrigger("FirstAttack"); });
    }

    public void PlaySecondAttack(float tick)
    {
        sequence.Kill();
        _animator.enabled = false;
        sequence = DOTween.Sequence();
        AttackState = AttackStates.SecondAttack;

        sequence.Append(_secondaryTransform.transform.DOLocalMove(new Vector2(0.98f, 0.55f), tick * 9));
        sequence.Join(_secondaryTransform.transform.DOLocalRotate(new Vector3(0, 0, -162.95f), tick * 9));
        sequence.Join(_primaryTransform.transform.DOLocalMove(new Vector2(-1.06f, -0.37f), tick * 9));
        sequence.Join(_primaryTransform.transform.DOLocalRotate(new Vector3(0, 0, 121.68f), tick * 9));

        sequence.OnComplete(() => { _animator.enabled = true; _animator.SetTrigger("SecondAttack"); });
    }

    public void PlaySwingFirstAttack(float tick)
    {
        sequence.Kill();
        _animator.speed = _animatorChargedSpeed; // ��� �� ���� ��������?
        _animator.enabled = false;
        sequence = DOTween.Sequence();
        AttackState = AttackStates.ChargeSwing;

        sequence.Append(_secondaryTransform.transform.DOLocalMove(new Vector2(-1.2303f, 0.611f), tick * 6));
        sequence.Insert(0, _secondaryTransform.transform.DOLocalRotate(new Vector3(0, 0, -33.34f), tick * 6));
        sequence.Insert(0, _primaryTransform.transform.DOLocalMove(new Vector2(0.82813f, -0.58f), tick * 6));
        sequence.Insert(0, _primaryTransform.transform.DOLocalRotate(new Vector3(0, 0, -137.27f), tick * 6));
    }

    public void PlayUltimativeFirstAttack()
    {
        _animator.enabled = true;
        _animator.SetTrigger("FirstAttack");
    }
}
