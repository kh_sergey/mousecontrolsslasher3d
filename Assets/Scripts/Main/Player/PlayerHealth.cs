using System;
using System.Collections;
using UnityEngine;
using DG.Tweening;

public class PlayerHealth : MonoBehaviour
{
    [SerializeField] private float _maxValue;

    private Player _player;
    private PlayerUI _playerUI;
    private float _value;

    public float MaxValue { get => _maxValue; private set => _maxValue = value; }
    public float Value { get => _value; private set { _value = value; _playerUI.UpdateCanvasInfo(); } }

    private void Awake()
    {
        _player = GetComponent<Player>();
        _playerUI = GetComponent<PlayerUI>();
    }

    private void Start()
    {
        Value = MaxValue;
    }

    public float GetHealthProcent()
    {
        return Value / MaxValue;
    }

    public void Damage(float damageValue)
    {
        Value -= damageValue;
        if (Value < 0)
        {
            Value = 0;
        }

        if (Value == 0)
        {
            _player.Die();
        }
    }

    public void Heal(float healValue)
    {
        Value += healValue;
        if (Value > MaxValue) Value = MaxValue;
    }
}
