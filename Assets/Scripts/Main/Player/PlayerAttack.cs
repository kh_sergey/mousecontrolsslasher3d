using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttack : MonoBehaviour
{
    [SerializeField] private float _basicAttackPower;
    [SerializeField] private float _chargeAttackPower;
    [SerializeField] private float _ultimativeAttackPower;
    [SerializeField] private float _basicKnockbackPower;
    [SerializeField] private float _chargeKnockbackPower;
    [SerializeField] private float _ultimativeKnockbackPower;
    [SerializeField] private float _basicAttackTick;
    [SerializeField] private float _chargeAttackTick;
    [SerializeField] private float _chargePointsForAttack;
    [SerializeField] private float _chargePointsForUltimativeAttack;
    [SerializeField] private LayerMask _raycastLayerMask;

    private Player _player;
    private TrailRenderer _bladeTrailRenderer;
    private Collider2D _bladeCollider;

    private PrimaryAttackAnimation _attackAnimation;
    private BounceCharge _bounceCharge;
    private GameObject _gameObjectPlayer;

    public bool UltimativeAttack { get; private set; } = false;// ��� ������� ������ �������������� ����������. �� � � ������� �������.
    public bool Charged { get; private set; }
    public bool AttackSlowdown { get; private set; } = false; // ���� ��� ���������� ��������. �������� true � ������ ��������������� ����� �������� � ������
    public float AttackPower { get; private set; }
    public float KnockbackPower { get; private set; }
    public float Tick { get; private set; }
    public float ChargePointsForAttack { get => _chargePointsForAttack; private set => _chargePointsForAttack = value; }
    public float ChargePointsForUltimativeAttack { get => _chargePointsForUltimativeAttack; private set => _chargePointsForUltimativeAttack = value; }

    private void Awake()
    {
        _player = GetComponentInParent<Player>();
        _bounceCharge = GetComponentInParent<BounceCharge>();
        _gameObjectPlayer = GetComponentInParent<Player>().gameObject;
        _attackAnimation = GetComponent<PrimaryAttackAnimation>();
        _bladeCollider = GetComponent<PolygonCollider2D>();
        _bladeTrailRenderer = GetComponentInChildren<TrailRenderer>();
    }

    public void Attack()
    {
        // ����� ���������� �����
        
        Charged = _bounceCharge.TryDecreaseValue(ChargePointsForAttack);
        if (Charged)
        {
            AttackPower = _chargeAttackPower;
            KnockbackPower = _chargeKnockbackPower;
            Tick = _chargeAttackTick;
        } else
        {
            AttackPower = _basicAttackPower;
            KnockbackPower = _basicKnockbackPower;
            Tick = _basicAttackTick;
        }
        
        SelectAttack();
    }

    public void SelectAttack()
    {
        _player.ActionPossibility = false;

        if (_attackAnimation.AttackState == PrimaryAttackAnimation.AttackStates.FirstAttack)
        {
            _attackAnimation.PlaySecondAttack(Tick);
        }
        else
        {
            _attackAnimation.PlayFirstAttack(Tick);
        }
    }

    public void StartUltimativeAttack()
    {
        AttackPower = _ultimativeAttackPower;
        KnockbackPower = _ultimativeKnockbackPower;

        _bladeTrailRenderer.enabled = true;
        _attackAnimation.PlaySwingFirstAttack(Tick);

        UltimativeAttack = true;
    }

    public void ExecuteUltimativeAttack()
    {
        UltimativeAttack = false;

        _attackAnimation.PlayUltimativeFirstAttack();
        _bounceCharge.TryDecreaseValue(ChargePointsForUltimativeAttack);
    }

    public void ToggleWeaponCollider(bool colliderStatus)
    {
        // ����� ����������� ��������� ������ - ����������� ����������� ������� ����

        AttackSlowdown = colliderStatus;
        if (colliderStatus)
        {
            if (Charged)
            {
                _bladeTrailRenderer.enabled = true;
                _attackAnimation.Animator.speed = _attackAnimation.AnimatorChargedSpeed;
            }
            _bladeCollider.enabled = true;
        }
        else
        {
            _bladeTrailRenderer.enabled = false;
            _bladeCollider.enabled = false;
            _player.ActionPossibility = true;
            _attackAnimation.Animator.speed = _attackAnimation.AnimatorDefaultSpeed;
        }
    }

    private void OnTriggerEnter2D(Collider2D collider)
    {
        // ���� �� ���������� � ���������� - ���� ������������ ���������� � ��������� ��������� ����.
        if (collider.GetComponentInParent<Enemy>() != null)
        {
            //Debug.Log("Find Enemy");
            // ���� ������ �� ����� - ������� ���. �������� � ���� - ������ ����, ����� ���� ������������
            Vector3 enemyDirection = (collider.transform.position - _player.transform.position).normalized;

            Debug.DrawRay(_player.transform.position, enemyDirection * 10f, Color.red);
            RaycastHit2D hit = Physics2D.Raycast(_player.transform.position, enemyDirection, 10f, _raycastLayerMask);

            if (hit)
            {
                //Debug.Log("hitted: "+ hit.collider.name);
            }

            if (hit.collider.tag != "Wall")
            {
                collider.GetComponentInParent<Enemy>().Hit(AttackPower, KnockbackPower, _gameObjectPlayer.transform.position, hit.collider.gameObject);
            }
        }
    }
}
