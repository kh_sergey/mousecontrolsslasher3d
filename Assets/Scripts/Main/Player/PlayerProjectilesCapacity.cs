using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class PlayerProjectilesCapacity : MonoBehaviour
{
    public int MaxValue { get => _maxValue; private set => _maxValue = value; }
    public int Value
    {
        get => _value;
        private set
        {
            _value = value;
            _playerUI.UpdateCanvasInfo();
        }
    }

    [SerializeField] private int _maxValue;

    private PlayerUI _playerUI;
    private int _value;

    private void Awake()
    {
        _playerUI = GetComponent<PlayerUI>();
    }

    private void Start()
    {
        Value = 0;
    }

    public void ResetValue()
    {
        Value = 0;
    }

    public bool TryDecreaseValue()
    {
        bool decreased = false;
        if (Value >= 1)
        {
            decreased = true;
            Value -= 1;
        } else
        {
            decreased = false;
        }

        return decreased;
    }

    public void IncreaseValue(int increaseAmount)
    {
        Value += increaseAmount;
        if (Value > MaxValue) Value = MaxValue;
    }
}
