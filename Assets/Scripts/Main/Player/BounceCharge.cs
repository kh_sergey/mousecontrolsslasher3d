using System;
using System.Collections;
using UnityEngine;
using DG.Tweening;

public class BounceCharge : MonoBehaviour
{
    public GameObject CurrentProjectileDummy { get; private set; }
    public float MaxValue { get => _maxValue; private set => _maxValue = value; }
    public float Value
    {
        get => _value;
        private set
        {
            _value = value;
            _playerUI.UpdateCanvasInfo();
            ToggleChargedProjectileVisuals(_value > 0);
        }
    }

    [SerializeField] private AudioSource _bounceChargeIncrease;
    [SerializeField] private AudioSource _bounceChargeOverflow;
    [SerializeField] private GameObject _projectileDefaultDummy;
    [SerializeField] private GameObject _projectileChargedDummy;
    [SerializeField] private float _maxValue;

    private Player _player;
    private PlayerUI _playerUI;
    private PlayerAttack _playerAttack;
    private PlayerProjectilesCapacity _projectilesCapacity;
    private float _value;

    private void Awake()
    {
        _player = GetComponent<Player>();
        _playerUI = GetComponent<PlayerUI>();
        _playerAttack = GetComponentInChildren<PlayerAttack>();
        _projectilesCapacity = GetComponent<PlayerProjectilesCapacity>();
        CurrentProjectileDummy = _projectileDefaultDummy;
    }

    private void Start()
    {
        Value = 0;
    }

    public float GetValueProcent()
    {
        return Value / MaxValue;
    }

    public bool TryDecreaseValue(float decreaseAmount)
    {
        bool decreased = false;
        float BounceChargeChecker = Value;
        Value -= decreaseAmount;

        if (Value < 0)
        {
            Value = 0;
        }

        if (BounceChargeChecker > Value)
        {
            decreased = true;
        }

        if (Value == 0)
        {
            if (_playerAttack.UltimativeAttack)
            {
                // ���� ���������� ����� � � ��� ����� ����������� ������������� ����� - ����� � �������� � ������ ����.
                _player.ExecuteUltimativeAttack();
            }
        }

        return decreased;
    }

    public void IncreaseValue(int increaseAmount)
    {
        Value += increaseAmount;
        if (Value > MaxValue) Value = MaxValue;
    }

    private void ToggleChargedProjectileVisuals(bool state)
    {
        if (_projectilesCapacity.Value > 0)
        {
            if (state) // �������� ���, ��� ����� ��� ����� ���������. ���?
            {
                _projectileDefaultDummy.SetActive(false);
                CurrentProjectileDummy = _projectileChargedDummy;
            }
            else
            {
                _projectileChargedDummy.SetActive(false);
                CurrentProjectileDummy = _projectileDefaultDummy;
            }
            CurrentProjectileDummy.SetActive(true);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "Wall" && !_playerAttack.UltimativeAttack && _player.ActionPossibility)
        {
            bool chargeTakePossibility = false;
            if (MaxValue > Value)
            {
                if (collision.collider.TryGetComponent(out ChargedWall Block))
                {
                    chargeTakePossibility = Block.TryIncreasePlayerCharge();
                }
            }

            if (chargeTakePossibility)
            {
                _bounceChargeIncrease.Play();
            }
            else
            {
                _bounceChargeOverflow.Play();
            }
        }
    }
}
