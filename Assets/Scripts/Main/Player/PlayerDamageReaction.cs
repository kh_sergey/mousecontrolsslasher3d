using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDamageReaction : MonoBehaviour
{
    [SerializeField] private float _invincibleTime;
    [SerializeField] private CircleCollider2D _physicsCollider;
    [SerializeField] private CircleCollider2D _takingHitTrigger;
    [SerializeField] private MeshRenderer _meshRenderer;

    private Player _player;
    private PlayerHealth _health;
    private Rigidbody2D _rigidbody;
    private Material _material;
    private DOTAnimations _DOTAnimations;

    public Task knockbackTask { get; private set; }
    public Task invincibilityTask { get; private set; }

    private void Awake()
    {
        _player = GetComponent<Player>();
        _health = GetComponent<PlayerHealth>();
        _rigidbody = GetComponentInChildren<Rigidbody2D>();
        _material = _meshRenderer.material;
        _DOTAnimations = GetComponent<DOTAnimations>();
    }

    public void Hit(float attackPower, float knockbackPower, Vector3 knockbackPointPosition)
    {
        if (invincibilityTask == null || !invincibilityTask.Running)
        {
            _player.StopAllMovementTasks();
            _player.ExecuteUltimativeAttack();
            MakeHitColor();
            _health.Damage(attackPower);
            knockbackTask = new Task(KnockbackTask(attackPower, knockbackPower, knockbackPointPosition));
            invincibilityTask = new Task(InvincibilityTask());
        }
    }

    private IEnumerator KnockbackTask(float attackPower, float knockbackPower, Vector3 knockbackPointPosition)
    {
        Vector2 knockbackDirection = (transform.position - knockbackPointPosition).normalized;
        _rigidbody.velocity = Vector2.zero;
        _rigidbody.velocity += knockbackDirection * knockbackPower;

        while (Mathf.Abs(_rigidbody.velocity.x) + Mathf.Abs(_rigidbody.velocity.y) > 1f)
        {
            _rigidbody.velocity = _rigidbody.velocity * 0.95f;

            yield return new WaitForFixedUpdate();
        }

        _rigidbody.velocity = Vector2.zero;
    }

    private void MakeHitColor() // ������-�� DOTween �������� ��������� ������ � IEnumerator. �������������� � ������ � ������� �������.
    {
        new Task(_DOTAnimations.MakeHitColor());
        //_DOTAnimations.MakeHitColor();
    }

    private void PlayInvincibilitySequence(float time)
    {
        new Task(_DOTAnimations.PlayInvincibilitySequence(time));
    }

    private IEnumerator InvincibilityTask()
    {
        PlayInvincibilitySequence(_invincibleTime);

        ToggleAllEnemyColliders(true);

        yield return new WaitForSeconds(_invincibleTime);

        ToggleAllEnemyColliders(false);
    }

    private void ToggleAllEnemyColliders(bool colliderStatus)
    {
        // ��������� ������� ��������� ����� ������
        _takingHitTrigger.enabled = !colliderStatus;
        // ��������� �������������� � ����������� ������������
        for (int i = 0; i < _player.EnemySet.GetItemsCount(); i++)
        {
            Physics2D.IgnoreCollision(_physicsCollider, _player.EnemySet.GetItemIndex(i).GetComponentInChildren<Collider2D>(), colliderStatus);
        }
    }
}
