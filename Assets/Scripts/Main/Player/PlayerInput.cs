using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInput : MonoBehaviour
{
    [SerializeField] private float _maxHoldTime;
    [SerializeField] GameObjectRuntimeSet _gameObjectGameManager;

    private Player _player;
    private PlayerUI _playerUI;
    private PlayerAttack _playerAttack;
    private BounceCharge _bounceCharge;
    private PlayerMovement _playerMovement;
    private GameManager _gameManager;

    private int _controlsScheme;
    private float _dashHoldTime;
    private float _holdDownStartTime;
    private bool _dash = false;

    public float DashHoldTime { get => _dashHoldTime; private set => _dashHoldTime = value; }
    public float MaxHoldTime { get => _maxHoldTime; private set => _maxHoldTime = value; }
    public bool AttackRegister { get; private set; }

    private void Awake()
    {
        _player = GetComponent<Player>();
        _playerUI = GetComponent<PlayerUI>();
        _playerAttack = GetComponentInChildren<PlayerAttack>();
        _bounceCharge = GetComponent<BounceCharge>();
        _playerMovement = GetComponent<PlayerMovement>();

        if (PlayerPrefs.HasKey("ControlsScheme"))
        {
            _controlsScheme = PlayerPrefs.GetInt("ControlsScheme"); // 0 - ���, 1 - MB4
        } else
        {
            _controlsScheme = 0;
        }
    }

    private void Start()
    {
        _gameManager = _gameObjectGameManager.GetItemIndex(0).GetComponent<GameManager>();
    }

    private void Update()
    {
        if (!_gameManager.IsPaused)
        {
            if (_controlsScheme == 0)
            {
                if (Input.GetKeyDown(KeyCode.Mouse2) && !_playerAttack.UltimativeAttack && _player.ActionPossibility)
                {
                    _player.Shoot();
                }
            } else
            {
                if (Input.GetKeyDown(KeyCode.Mouse4) && !_playerAttack.UltimativeAttack && _player.ActionPossibility)
                {
                    _player.Shoot();
                }
            }

            if (Input.GetMouseButtonDown(0) && !_playerAttack.UltimativeAttack)
            {
                _holdDownStartTime = Time.time;
                _dash = true;
            }

            if (Input.GetMouseButton(0) && _player.ActionPossibility && _dash)
            {
                DashHoldTime = Time.time - _holdDownStartTime;
                _playerUI.ShowForce(DashHoldTime);

                if (Input.GetMouseButtonDown(1))
                {
                    if (_bounceCharge.Value > 0.25f)
                    {
                        _playerUI.HideForce();
                        _dash = false;

                        _player.StartUltimativeAttack();
                    }
                }
            }

            if (Input.GetMouseButtonUp(0) && _player.ActionPossibility && _dash)
            {
                if (DashHoldTime > MaxHoldTime)
                    DashHoldTime = MaxHoldTime;
                _player.LongDashToPosition(DashHoldTime, Camera.main.ScreenToWorldPoint(Input.mousePosition));
            }
            else if (Input.GetMouseButtonUp(0) && (!_player.ActionPossibility || !_dash))
            {
                _playerUI.HideForce();
            }

            if (Input.GetMouseButtonUp(1) && _playerAttack.UltimativeAttack)
            {
                _player.ExecuteUltimativeAttack();

                _dash = false;
            }

            if (Input.GetMouseButtonDown(1) && !_playerAttack.UltimativeAttack)
            {
                AttackRegister = true;
            }

            if (AttackRegister == true && _player.ActionPossibility)
            {
                AttackRegister = false;
                _player.Attack();

                _dash = false;
                _playerUI.HideForce();
            }
        }
    }

    private void FixedUpdate()
    {
        if (!_gameManager.IsPaused)
        {
            _playerMovement.RotateToPosition(Camera.main.ScreenToWorldPoint(Input.mousePosition));
        }
    }
}
