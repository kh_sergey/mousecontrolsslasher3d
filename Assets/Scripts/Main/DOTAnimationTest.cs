using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DOTAnimationTest : MonoBehaviour
{
    //private Material _material;
    private DOTAnimations _DOTAnimations;

    private void Awake()
    {
        //_material = GetComponent<MeshRenderer>().material;
        _DOTAnimations = GetComponent<DOTAnimations>();
    }

    private void OnMouseDown()
    {
        new Task(_DOTAnimations.MakeHitColor());
    }
}
