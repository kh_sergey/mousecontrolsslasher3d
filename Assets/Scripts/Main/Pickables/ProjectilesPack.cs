using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class ProjectilesPack : MonoBehaviour
{
    [SerializeField] private int _projectilesInPack;
    private AudioSource _projectilesPackPickupSound;

    private void Awake()
    {
        _projectilesPackPickupSound = GameObject.Find("BounceChargeIncrease").GetComponent<AudioSource>(); // ������ �� ����� ������ �� ����� - ����� �����. ��:
    }                                                                                            // ��� ������������ ������. �������� � ���� ��������� - �� ����� ���������

    /*
    private void Start()
    {
        new Task(RotatePack());
    }

    private IEnumerator RotatePack()
    {
        while (true)
        {
            gameObject.transform.DORotate(new Vector3(0f, 180f, 0f), 5f, RotateMode.FastBeyond360);
            yield return new WaitForFixedUpdate();
        }
    }
    */

    // � �� ������� ����� SerializeField ������ ������� �� ��������� �� �����
    private void OnTriggerEnter2D(Collider2D collider)                                           // ����� �� ������� �����?
    {
        if (collider.GetComponentInParent<Player>() != null)
        {
            if (collider.GetComponentInParent<PlayerProjectilesCapacity>().Value < 3)
            {
                _projectilesPackPickupSound.Play();
                collider.GetComponentInParent<PlayerProjectilesCapacity>().IncreaseValue(_projectilesInPack);
                Destroy(gameObject);
            }
        }
    }
}
