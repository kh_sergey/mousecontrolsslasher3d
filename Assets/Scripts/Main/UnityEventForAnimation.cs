using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;

public class UnityEventForAnimation : MonoBehaviour
{
    [SerializeField] private UnityEvent _event;

    public void InvokeEvent()
    {
        _event.Invoke();
    }
}
