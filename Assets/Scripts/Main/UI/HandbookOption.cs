using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class HandbookOption : MonoBehaviour
{
    [TextArea(5, 20)]
    [SerializeField] private string _infoText;
    [SerializeField] private TextMeshProUGUI _infoPanel;

    private bool _checking = false; // Невероятные костыли
    private float _beforeNewTextPosition;
    private RectTransform _rectTransform;

    private void Awake()
    {
        _rectTransform = _infoPanel.GetComponent<RectTransform>();
    }

    public void ApplyTextOnPanel()
    {
        _beforeNewTextPosition = _rectTransform.sizeDelta.y;
        _infoPanel.text = _infoText;
        _rectTransform.anchorMin = new Vector2(0, 1);
        _rectTransform.anchorMax = new Vector2(0, 1);
        _rectTransform.pivot = new Vector2(0.5f, 0.5f);

        _checking = true;
    }

    private void Update()
    {
        if (_checking)
        {
            if (_beforeNewTextPosition != _rectTransform.sizeDelta.y) {
                _rectTransform.localPosition = new Vector2(_rectTransform.localPosition.x, -(_rectTransform.sizeDelta.y / 2));
                _checking = false;
            }
        }
    }
}
