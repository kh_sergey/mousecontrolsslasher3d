using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonInvoke : MonoBehaviour
{
    public void InvokeButton(Button button)
    {
        button.onClick.Invoke();
    }
}
