using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour
{
    private string _chosenLevelToLoad = "none";

    private void Awake()
    {
        //PlayerPrefs.DeleteKey("MaxAchievedLevel");
        //PlayerPrefs.SetInt("MaxAchievedLevel", 1);
        //Debug.Log(PlayerPrefs.GetInt("MaxAchievedLevel"));
    }

    /*
    public void ContinueGameButton()
    {
        if (!PlayerPrefs.HasKey("MaxAchievedLevel"))
        {
            PlayerPrefs.SetInt("MaxAchievedLevel", 1); 
        }

        _chosenLevelToLoad = "Level" + PlayerPrefs.GetInt("MaxAchievedLevel");
        LoadChosenLevel();
    }
    */

    public void ChooseLevelToLoad(string levelToLoad)
    {
        _chosenLevelToLoad = levelToLoad;
    }

    public void LoadChosenLevel()
    {
        PlayerPrefs.SetString("Arena", "Arena1");
        PlayerPrefs.SetInt("PlayCutcsene", 1);
        SceneManager.LoadScene(_chosenLevelToLoad);
    }

    public void ExitButton()
    {
        Application.Quit();
    }
}
