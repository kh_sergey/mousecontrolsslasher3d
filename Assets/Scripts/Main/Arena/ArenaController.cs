using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class ArenaController : MonoBehaviour
{
    public GameObjectRuntimeSet CurrentArenaControllerRuntimeSet;

    [SerializeField] private GameObjectRuntimeSet _gameManagerRuntimeSet;
    [SerializeField] private ArenaController _nextArenaController;
    [SerializeField] private GameObjectRuntimeSet _enemyRuntimeSet;
    [SerializeField] private GameObjectRuntimeSet _playerRuntimeSet;
    [SerializeField] private GameObjectRuntimeSet _cameraRuntimeSet;
    [SerializeField] private GameObjectRuntimeSet _dialogueManagerRuntimeSet;
    [SerializeField] private Transform _playerSpawnPosition;
    [SerializeField] private Transform _cameraPosition;
    [SerializeField] private Dialogue _dialogue;
    private List<Door> _doors;
    private CameraController _cameraController;
    private Task _doorsTask;
    private Task _cutsceneTask;
    private TrailRenderer _playerTrailRenderer;
    private GameObject _playerGameObject;
    private GameObject _cameraGameObject;
    private GameManager _gameManager;
    private bool UpperDoorOpened = false;
    private PlayerHealth _playerHealth;
    private BounceCharge _playerCharge;
    private PlayerProjectilesCapacity _playerProjectilesCapacity;

    public ArenaController NextArenaController { get => _nextArenaController; private set => _nextArenaController = value; }
    public GameObjectRuntimeSet EnemySet { get => _enemyRuntimeSet; private set => _enemyRuntimeSet = value; }
    public GameObjectRuntimeSet PlayerSet { get => _playerRuntimeSet; private set => _playerRuntimeSet = value; }
    public GameObjectRuntimeSet CameraSet { get => _cameraRuntimeSet; private set => _cameraRuntimeSet = value; }
    public Transform PlayerSpawnPosition { get => _playerSpawnPosition; private set => _playerSpawnPosition = value; }
    public Transform CameraPosition { get => _cameraPosition; private set => _cameraPosition = value; }

    private void Awake()
    {
        _doors = new List<Door>(GetComponentsInChildren<Door>());
    }

    private void Start()
    {
        _gameManager = _gameManagerRuntimeSet.GetItemIndex(0).GetComponent<GameManager>();
        _playerGameObject = _playerRuntimeSet.GetItemIndex(0);
        _cameraGameObject = _cameraRuntimeSet.GetItemIndex(0);
        _cameraController = _cameraGameObject.GetComponent<CameraController>();
        _playerTrailRenderer = _playerGameObject.GetComponent<TrailRenderer>();
        _playerHealth = _playerGameObject.GetComponent<PlayerHealth>();
        _playerCharge = _playerGameObject.GetComponent<BounceCharge>();
        _playerProjectilesCapacity = _playerGameObject.GetComponent<PlayerProjectilesCapacity>();
        CurrentArenaControllerRuntimeSet.Initialize();

        ResetPlayerStats();
    }

    private void Update() // ��������� ������ ���� - ������ ����. ������ ���� �����-�� ����� ������ ���� ����������� �� �����
    {
        if (!UpperDoorOpened)
        {
            if (_enemyRuntimeSet.GetItemsCount() == 0)
            {
                _doors[0].OpenDoor(); // ��������� ������� �����
                UpperDoorOpened = true;
            }
        }
    }

    public void LoadArena()
    {
        _playerTrailRenderer.enabled = false;
        _gameManager.TogglePause(true);

        UpdateCameraPosition();
        UpdatePlayerPosition();

        CurrentArenaControllerRuntimeSet.Initialize();
        CurrentArenaControllerRuntimeSet.AddToList(gameObject);

        // �������� ����� ������
        ResetPlayerStats();

        if (gameObject.name == "Arena1") // ������������ ������� ������
        {
            _cameraController.PlayEnterLevelAnimation();
        } else
        {
            StartPlayerWalkInAnimation();
        }
    }

    public void LoadNextArena()
    {
        PlayerPrefs.SetInt("PlayCutcsene", 1);
        PlayerPrefs.SetString("Arena", NextArenaController.gameObject.name);
        NextArenaController.LoadArena();
    }

    private void StartPlayerWalkInAnimation()
    {
        _cameraController.PlayEnterArenaAnimation();
        _doorsTask = new Task(OpenAndCloseDoorTask());
        _playerGameObject.transform.rotation = Quaternion.Euler(90f, 0f, 0f);
        _playerGameObject.GetComponent<Animator>().SetTrigger("WalkInArena");
    }

    public void TryPlayCutscene()
    {
        TogglePlayerInput(false);
        if (_dialogue.Sentences.Length > 0 && PlayerPrefs.GetInt("PlayCutcsene") == 1)
        {
            _cameraController.PlayZoomInCutsceneAnimation();
            _dialogueManagerRuntimeSet.GetItemIndex(0).GetComponent<DialogueManager>().StartDialogue(_dialogue);
        } else
        {
            TogglePlayerInput(true);
            _cameraController.ToggleHUD(1);
        }
    }

    public void TogglePlayerInput(bool toggle)
    {
        _playerTrailRenderer.enabled = toggle;
        _gameManager.TogglePause(!toggle);
    }

    public void EnterLevel()
    {
        TogglePlayerInput(false);
        _playerGameObject.transform.rotation = Quaternion.Euler(90f, 0f, 0f);
    }

    private IEnumerator OpenAndCloseDoorTask()
    {
        float time = 0f;
        float timeWindow = 1f;

        _doors[1].OpenDoor();

        while (time < timeWindow)
        {
            time += Time.deltaTime;
            yield return new WaitForFixedUpdate();
        }

        _doors[1].CloseDoor();
    }

    private void UpdatePlayerPosition()
    {
        _playerGameObject.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        _playerGameObject.transform.position = PlayerSpawnPosition.position;
    }

    private void UpdateCameraPosition()
    {
        _cameraGameObject.transform.root.position = CameraPosition.position;
    }

    private void ResetPlayerStats()
    {
        _playerHealth.Heal(_playerHealth.MaxValue);
        _playerCharge.TryDecreaseValue(3);
        _playerProjectilesCapacity.ResetValue();
    }

    public void ChooseCameraExitAnimation()
    {
        if (NextArenaController != null)
        {
            _cameraController.PlayExitArenaAnimation();
        }
        else
        {
            _cameraController.PlayExitLevelAnimation();
        }
    }
}
