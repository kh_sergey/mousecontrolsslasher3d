using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitArena : MonoBehaviour
{
    private ArenaController _arenaController;

    private void Start()
    {
        _arenaController = GetComponentInParent<ArenaController>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        _arenaController.ChooseCameraExitAnimation();
    }
}
