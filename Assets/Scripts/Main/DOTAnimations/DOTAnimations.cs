using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.Events;

public class DOTAnimations : MonoBehaviour
{
    [SerializeField] private MeshRenderer _changableRenderer;
    public Color OriginalColor { get; private set; }
    public Material Material { get; private set; }

    private void Awake()
    {
        OriginalColor = _changableRenderer.material.color;
        Material = _changableRenderer.material;
    }

    public IEnumerator MakeHitColor()
    {
        Sequence colorSequence = DOTween.Sequence();
        colorSequence.Append(Material.DOColor(new Color(0.8980f, 0.1160f, 0f), 0.2f));
        colorSequence.Append(Material.DOColor(OriginalColor, 0.2f));
        yield return new WaitForFixedUpdate();
    }

    public IEnumerator TransitBodyColor(Color newColor, float time)
    {
        Material.DOColor(newColor, time);
        yield return new WaitForFixedUpdate();
    }

    public IEnumerator MakePushedColor()
    {
        Sequence colorSequence = DOTween.Sequence();
        colorSequence.Append(Material.DOColor(new Color(0.2915628f, 0.9811321f, 0.575f), 0.2f));
        colorSequence.Append(Material.DOColor(OriginalColor, 0.2f));
        yield return new WaitForFixedUpdate();
    }

    public IEnumerator PlayInvincibilitySequence(float time)
    {
        Sequence invincibilitySequence = DOTween.Sequence();
        invincibilitySequence.Append(Material.DOFade(0.2f, time / 12));
        invincibilitySequence.Append(Material.DOFade(1f, time / 4));
        invincibilitySequence.Append(Material.DOFade(0.2f, time / 12));
        invincibilitySequence.Append(Material.DOFade(1f, time / 4));
        invincibilitySequence.Append(Material.DOFade(0.2f, time / 12));
        invincibilitySequence.Append(Material.DOFade(1f, time / 4));
        yield return new WaitForFixedUpdate();
    }
    
    public IEnumerator MakeDeathColor()
    {
        Material.DOColor(new Color(0.4811f, 0.4811f, 0.4811f), 0.4f);
        yield return new WaitForFixedUpdate();
    }
}
