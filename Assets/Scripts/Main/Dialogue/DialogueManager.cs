using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DialogueManager : MonoBehaviour
{
	[SerializeField] private GameObjectRuntimeSet _cameraControllerRuntimeSet;
	[SerializeField] private TextMeshProUGUI _nameText;
	[SerializeField] private TextMeshProUGUI _dialogueText;
	[SerializeField] private Animator _animator;
	private Task _sentenceTask;

	private Queue<string> _sentences;

	private void Start()
	{
		_sentences = new Queue<string>();
	}

	public void StartDialogue(Dialogue dialogue)
	{
		_animator.SetTrigger("ShowBox");

		_nameText.text = dialogue.Name;

		_sentences.Clear();

		foreach (string sentence in dialogue.Sentences)
		{
			_sentences.Enqueue(sentence);
		}

		DisplayNextSentence();
	}

	public void DisplayNextSentence()
	{
		if (_sentences.Count == 0)
		{
			EndDialogue();
			return;
		}

		string sentence = _sentences.Dequeue();
		_sentenceTask?.Stop();
		_sentenceTask = new Task(TypeSentence(sentence));
	}

	private IEnumerator TypeSentence(string sentence)
	{
		_dialogueText.text = "";
		foreach (char letter in sentence.ToCharArray())
		{
			_dialogueText.text += letter;
			yield return null;
		}
	}

	private void EndDialogue()
	{
		_animator.SetTrigger("HideBox");
		_cameraControllerRuntimeSet.GetItemIndex(0).GetComponent<CameraController>().PlayZoomOutCutsceneAnimation();
		PlayerPrefs.SetInt("PlayCutcsene", 0);
	}
}
